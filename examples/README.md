# SpDM^3 #
General steps to use SpDM^3:

* Include header file(s).
* Create SpDM^3 communicators (indicating matrix layout and replication factor).
* Create distributed matrices using the communicators.
* Call multiplication routines on one distributed matrices, passing the other two operands as arguments.

See examples in col.cpp, innerA.cpp, and row.cpp. Use "-h" option to see how to run them.
> make innerA
> mpirun -n 4 ./innerA -h

## Header Files ##
* Include _spdm3.h_ for all SpDM^3 routines.
* (Optional) Include _flush.h_ for a mini cache-flushing tool (for benchmarking). 

## Creating Communicators ##
Use GetComms to create communicators to specify partitioning for a matrix. The created communicators can be reused with multiplie matrices. The user must free the communicators manually when the matrices depending on them are not going to be used anymore. Format:
* GetComms(MPI_COMM_WORLD, <spdm3_layout>, <replication factor>);

Example:

* Comm *commA = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, cA);
* Comm *commB = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, cB);
* DistSpMat<IT, VT> A(commA);
* DistDMat<IT, VT>  B(commB);
* DistDMat<IT, VT>  C(commA);
* ...
* (A, B, C, and other matrices depending on commA and comm B cannot be used from this point onward.)
* delete [] comm1;
* delete [] comm2;
* (The delete commands must be before MPI_Finalize().)
* MPI_Finalize();


## Matrix-Matrix Multiplication Routines ##
Currently there are 3 routines,

* MMRow(A, B, C, T): All matrices in block-row layout. Rotates B.
* MMCol(A, B, C, T): All matrices in block-column layout. Rotates A.
* MMInnerA(A, B, C, T): A in block-row layout. B and C in block-column layout. Rotates A.

They all calculate C = AB. 
A can be sparse or dense. We assume B and C are dense.
T is the timer (see the Timer section below).


## Helper Functions ##
### Parsing Command Line Arguments ###
* **read\_int(argc, argv, <option\_string>, <default\_value>)** returns **int**
* **read\_double(argc, argv, <option\_string>, <default\_value>)** returns **double**
* **read\_string(argc, argv, <option\_string>, <default\_value>)** returns **char \***
* **find\_option(argc, argv, <option\_string>)** returns the index of argv with the option string.

For example, for
>./your_prog -i input.npy -c 2 -l1 0.5 -debug,

you could use

* char \*input_filename = read_string(argc, argv, "-i", "default.npy");
* int c = read_int(argc, argv, "-c", 1);
* double l1 = read_double(argc, argv, "-l1", 0.1);
* bool debug = (find_option(argc, argv, "-debug") > 0);

## Flushing Cache ##
To use the small flush tool in _flush.h_, 

* Initialize the flush tool at the beginning by calling _init_dummy()_.
* Call _flush_cache()_ whenever you would like to flush cache.
* Finalize the tool at the end of the program by calling _clear_dummy()_.

Since the cache size varies on different machines, you might need to adjust the variable _dummy_size_ in _flush.h_ accordingly for the tool to work.

## Timer ##
Our Timer relies on MPI, so you must compile with MPI.

* Declare the Timer at the beginning: _Timer T;_
* Initialize the timer as soon as MPI is initialized: _T.Init(MPI\_COMM\_WORLD);
* Call _T.Start("<timer\_name>");_ to start the timer named <timer\_name>.
* Call _T.Stop("<timer\_name>");_ to stop the timer named <timer\_name>.
* Call _T.Report()_ to see the values of all timers.
 
