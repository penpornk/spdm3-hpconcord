#ifndef __SPDM3_DISTIO_H__
#define __SPDM3_DISTIO_H__

#include "comm.h"
#include "distdmat.h"
#include "distspmat.h"

namespace spdm3 {

template <class IT, class VT>
DistDMat<IT, VT> LoadNumPy(const char *filename);

}  // namespace spdm3

#include "distio.cpp"

#endif  // __SPDM3_DISTIO_H__