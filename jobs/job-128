#!/bin/bash -l
# Example Edison job script.
# Runs on 128 nodes (2 MPI processes per node, 12 OpenMP threads per process)
# Has a time limit of 30 minutes.
# The ista5 variant we are running takes the sample covariance matrix (S)
# as the input, hence the input name S.npy.

#SBATCH -p debug            # Job queue (debug queue)
#SBATCH -N 128              # Number of nodes (128 nodes)
#SBATCH -t 00:30:00         # Time (30 minutes)
#SBATCH -J name             # Job name
#SBATCH -o name.%j.log      # Output file name
#SBATCH -A <repo>           # Allocation repo

set -x                      # Makes the script echo the commands

# Number of MPI processes (256 processes)
# This is equal to 2 * number of nodes
# because each node has 2 NUMA regions.
P=256
T=12              # Number of threads (12 cores per NUMA node)
PROG="./ista5"    # Path to the ista5 executable
INPUT="S.npy"     # Path to the sample covariance matrix input
L1=0.48           # Lambda_1 penalty
L2=0.39           # Lambda_2 penalty
TAU=0.125         # Starter tau
EPS=0.0001        # Epsilon (convergence criteria)

# Replication factor.
# Does not change the output matrix, but tries to reduce communication time.
# The optimal value is close to the largest power of two that is still
# less than sqrt(P). Here, sqrt(256) = 16 so the best replication factor
# is likely 8 or 16 (or sometimes 4).
# Use 1 for no replication.
C=8

# Fixes the clock frequency at 2.4MHz
# to prevent the Turbo Boost technology making
# the performance numbers inconsistent between runs.
F="--cpu-freq=2400000"

# OpenMP settings
export OMP_NUM_THREADS=$T
export KMP_AFFINITY="scatter,granularity=fine"

# Runs the program (but does not save the output).
# Set -o option to save the output.
date
srun -n $P -c $T $F ./${PROG} -i ${INPUT} -l1 ${L1} -l2 ${L2} -tau ${TAU} -eps ${EPS} -c ${C} -v -max_outer 100
date
