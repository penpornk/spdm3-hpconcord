#!/bin/bash

set -x

python submat.py f10x918.npy f$1x$2.npy $1 $2
python submat.py d10x918.npy d$1x$2.npy $1 $2

python pyconcord.py f$1x$2.npy > d$1x$2.log
mv Omega.out d$1x$2.out

mpirun -n 4 ./double -i d$1x$2.npy > double.log
mv Omega double.out
mpirun -n 1 ./concord -i f$1x$2.npy > float.log
mv Omega float.out

./sp2dmat double.out tmp
./diffdmat d$1x$2.out tmp
./sp2dmat float.out tmp
./diffdmat d$1x$2.out tmp

head -20 double.log
echo ""
head -20 float.log
echo ""

grep nan float.log
grep inf float.log