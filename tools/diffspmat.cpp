#include <iostream>
#include "spmat.h"

#define IT int
#define VT double

using namespace spdm3;

int main(int argc, char *argv[]) {
  SpMat<IT, VT> a, b;
  a.Load(argv[1]);
  b.Load(argv[2]);
  
  a.SetEpsilon(0.0001);
  if (a.IsEquivalent(b))
    std::cout << "Equal." << std::endl;
//  else {
//    a.Print();
//    std::cout << std::endl;
//    b.Print();
//  }
  return 0;
}