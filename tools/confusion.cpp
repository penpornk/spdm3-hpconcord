#include <cstdio>
#include "spdm3.h"

#define IT  int
#define VT  CMDVT

using namespace spdm3;

int main(int argc, char *argv[]) {
  if (argc < 3) {
    printf("Usage: %s <first_matrix> <second_matrix>\n", argv[0]);
    exit(1);
  }
  
  SpMat<IT, VT> A, B;
  A.LoadMatrixMarket(argv[1]);
  B.LoadMatrixMarket(argv[2]);
 
  int max_nnz = std::max(A.nnz_, B.nnz_);
  assert(A.rows_ == B.rows_);
  assert(A.cols_ == B.cols_);
  
  SpMat<IT, VT> intersect, A_B, B_A;
  intersect.Allocate(A.rows_, A.cols_, max_nnz);
  A_B.Allocate(A.rows_, A.cols_, max_nnz);
  B_A.Allocate(A.rows_, A.cols_, max_nnz);
  
  intersect.nnz_ = A_B.nnz_ = B_A.nnz_ = 0;
  intersect.headptrs_[0] = A_B.headptrs_[0] = B_A.headptrs_[0] = 0;
  for (int i = 0; i < A.dim1(); ++i) {
    int k_a = A.headptrs_[i];
    int k_b = B.headptrs_[i];
    while (k_a < A.headptrs_[i+1] && k_b < B.headptrs_[i+1]) {
      int j_a = A.indices_[k_a];
      int j_b = B.indices_[k_b];
      if (j_a < j_b) {
        A_B.indices_[A_B.nnz_]  = j_a;
        A_B.values_[A_B.nnz_++] = A.values_[k_a++];
      } else if (j_b < j_a) {
        B_A.indices_[B_A.nnz_]  = j_b;
        B_A.values_[B_A.nnz_++] = B.values_[k_b++];
      } else {
        intersect.indices_[intersect.nnz_]  = j_a;
        intersect.values_[intersect.nnz_++] = 1.0;
        ++k_a;
        ++k_b;
      }
    }
    while (k_a < A.headptrs_[i+1]) {
      A_B.indices_[A_B.nnz_]  = A.indices_[k_a];
      A_B.values_[A_B.nnz_++] = A.values_[k_a++];
    }
    while (k_b < B.headptrs_[i+1]) {
      B_A.indices_[B_A.nnz_]  = B.indices_[k_b];
      B_A.values_[B_A.nnz_++] = B.values_[k_b++];
    }
    intersect.headptrs_[i+1] = intersect.nnz_;
    A_B.headptrs_[i+1] = A_B.nnz_;
    B_A.headptrs_[i+1] = B_A.nnz_;
  }
  
  intersect.SaveMatrixMarket("in_both.mtx");
  A_B.SaveMatrixMarket("a-b.mtx");
  B_A.SaveMatrixMarket("b-a.mtx");
  
  printf("%d %d %d\n", intersect.nnz_, A_B.nnz_, B_A.nnz_);

  return 0;
}
