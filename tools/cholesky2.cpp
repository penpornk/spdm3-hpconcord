#include <cstdio>
#include "spdm3.h"

#define IT  int
#define VT  CMDVT

using namespace spdm3;

int main(int argc, char *argv[]) {
  assert(argc == 2);

  lapack_int info;
  double start = read_timer();
  DMat<IT, VT> A(DENSE_COLMAJOR);
  A.LoadNumPy(argv[1]);
  printf("[%10.4lf] A is read.\n", read_timer() - start);
  for (VT offset = 0.0; offset <= 0.1; offset += 0.01) {
    DMat<IT, VT> B(DENSE_COLMAJOR);
    B.DeepCopy(A);
    for (int i = 0; i < B.rows_; ++i) {
      B.values_[i * B.lda_ + i] += offset;
    }
    info = LAPACKE_spotrf(LAPACK_ROW_MAJOR, 'U', B.rows_, B.values_, B.lda_);
    printf("[%10.4lf] offset = %lf, info = %d\n", read_timer() - start, offset, info);
  }
  return 0;
}
