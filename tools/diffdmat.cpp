#include <iostream>
#include "dmat.h"

#define IT int
#define VT double

using namespace spdm3;

int main(int argc, char *argv[]) {
  DMat<IT, VT> a, b;
  a.Load(argv[1]);
  b.Load(argv[2]);
  a.SetEpsilon(0.001);
  if (a.IsEquivalent(b))
    std::cout << "Equal." << std::endl;
  else {
    a.Print();
    std::cout << std::endl;
    b.Print();
  }
  return 0;
}