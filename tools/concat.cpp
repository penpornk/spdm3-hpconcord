#include <stdio.h>

#include "omp.h"
#include "spdm3.h"

#define IT  int
#define VT  CMDVT

using namespace spdm3;

// Very simple matrix concatenation code.
// Matrix file format: npy, column-major.
// Concatenates columns.
// Usage: ./concat <input_1> ... <input_n> <output>
int main(int argc, char *argv[]) {
  double timer, start = omp_get_wtime();
  int64_t num_elements = 0;

  // Counts total number of columns.
  int global_rows = -1, global_cols = 0;
  for (int i = 1; i < argc-1; ++i) {
    unsigned int *shapes, ndims;
    get_npy_shapes(argv[i], shapes, ndims);
    if (global_rows == -1)
      global_rows = shapes[0];
    else
      assert(global_rows == shapes[0]);
    global_cols += shapes[1];
    delete [] shapes;
  }
  
  // Writes the npy header.
  unsigned int shapes[2];
  shapes[0] = global_rows;
  shapes[1] = global_cols;
  FILE *f_out = fopen(argv[argc-1], "wb");
  write_npy_header<VT>(f_out, NULL, shapes, 2, true);
  
  // Writes file-by-file.
  for (int i = 1; i < argc-1; ++i) {
    unsigned int shapes[2];
    DMat<IT, VT> in(DENSE_COLMAJOR);
    timer = -omp_get_wtime();
    in.LoadNumPy(argv[i]);
    timer += omp_get_wtime();
    printf("[%10.4lf] %03d: Loaded in %lf seconds\n",
        omp_get_wtime() - start, i, timer);
  
    timer = -omp_get_wtime();
    num_elements = (int64_t) in.rows_ * (int64_t) in.cols_;
    fwrite(in.values_, sizeof(VT), num_elements, f_out);
    timer += omp_get_wtime();
    printf("[%10.4lf] %03d:  Wrote in %lf seconds\n",
        omp_get_wtime() - start, i, timer);
  }
  fclose(f_out);
  return 0;
}
