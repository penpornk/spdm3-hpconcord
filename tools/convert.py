import numpy as np
import sys

x = np.load(sys.argv[1])
if sys.argv[3] == "f":
  np.save(sys.argv[2], x.astype(np.float32))
else:
  np.save(sys.argv[2], x.astype(np.float64))


