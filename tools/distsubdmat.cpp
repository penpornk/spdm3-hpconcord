#include "spdm3.h"

#define IT  int
#define VT  CMTVT

using namespace spdm3;

int main(int argc, char *argv[]) {
  int rank, procs;
  
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  
  
  MPI_Finalize();
  return 0;
}