#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <numeric>
#include <random>
#include <vector>
#include <omp.h>

#include "spdm3.h"

using namespace spdm3;

#define IT int
#define VT CMDVT

int main(int argc, char *argv[]) {
  if (argc < 3) {
    printf("Usage: %s <input> <output> [<seed>]\n", argv[0]);
    exit(1);
  }
  char *input = argv[1];
  char *output = argv[2];
  unsigned int seed = (argc == 4)
                      ? (unsigned int) atoi(argv[3])
                      : (unsigned int) time(NULL);
  srand(seed);
  
  // Reads the matrix.
  double read_time = -omp_get_wtime();
  DMat<IT, VT> X(DENSE_COLMAJOR);
  X.LoadNumPy(input);
  read_time += omp_get_wtime();
  
  // Generates new row mappping.
  int n = X.cols_;
  double gen_time = -omp_get_wtime();
  std::vector<int> perm(n);
  my_iota(perm.begin(), perm.end(), 0);
  std::random_shuffle(perm.begin(), perm.end());
  FILE *fpermute = fopen("permutation.txt", "w");
  for (int i = 0; i < perm.size(); ++i)
    fprintf(fpermute, "%3d ", perm[i]);
  fprintf(fpermute, "\n");
  fclose(fpermute);
  gen_time += omp_get_wtime();
  
  
  // Allocates the new matrix.
  DMat<IT, VT> Y(DENSE_COLMAJOR);
  Y.StructuralCopy(X);
  
  // Shuffles the column.
  double shuffle_time = -omp_get_wtime();
  VT *src = X.values_;
  int col_size_in_bytes = X.rows_ * sizeof(VT);
  for (int i = 0; i < X.cols_; ++i) {
    memcpy(Y.values_ + perm[i] * X.rows_,
           src, col_size_in_bytes);
    src += X.rows_;
  }
  shuffle_time += omp_get_wtime();
  
  // Saves the output.
  double write_time = -omp_get_wtime();
  Y.SaveNumPy(output);
  write_time += omp_get_wtime();
  
  // Reports timings.
  printf("Read:       %lf seconds\n", read_time);
  printf("Generate:   %lf seconds\n", gen_time);
  printf("Shuffle:    %lf seconds\n", shuffle_time);
  printf("Write:      %lf seconds\n", write_time);
  
  return 0;
}
