header = """#!/bin/bash -l

#SBATCH -p debug
#SBATCH -N {nodes}
#SBATCH -t 00:30:00
#SBATCH -J {name}
#SBATCH -o {name}.%j.log
#SBATCH -A m888

module load ipm
set -x
export IPM_REPORT=full

P={p}
T={threads}
N=65536
PR=1.09
PNNZ=0.1
F="--cpu-freq=2400000" # To handle Turbo Boost
INPUT="$SCRATCH/data/d1200x110k.npy"

export OMP_NUM_THREADS=$T
export KMP_AFFINITY="scatter,granularity=fine"

"""

runs_per_file_base = 4
scaling_factor = 1.0
threads = 12
procs_per_node = 24 / threads

for p in [32, 64, 128, 256, 512]:
  nodes = int((p+procs_per_node-1)/ procs_per_node)
  c_list = list()
  crow = 1
  while crow <= p:
    ccol = 1
    while crow * ccol <= p:
      c_list.append((crow, ccol))
      ccol = ccol * 2
    crow = crow * 2
  name = "p%d" % (p * threads)
  with open(name, "w") as fout:
    print >>fout, header.format(nodes=nodes, name=name, p=p, threads=threads)

    crow = 1
    while crow <= p:
      ccol = 1
      while crow * ccol <= p:
        print >>fout, "date"
        print >>fout, \
            "srun -n $P -c $T $F ../allblkrow -n ${N} -pr ${PR} -pnnz ${PNNZ} -ca %d -cb %d" \
                % (crow, ccol)
        ccol = ccol * 2
      crow = crow * 2

    print >>fout, "date"


