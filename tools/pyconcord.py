from concord import concord
import numpy as np
import sys

x = np.load(sys.argv[1])
print np.round(x, 4)
omega = concord(x, 0.3, penalty2 = 0.2)

print np.round(omega.todense(), 4)
