#include <cstdio>
#include <cstdlib>
#include "mkl.h"
#include "spdm3.h"

using namespace spdm3;

#define IT  int
#define VT  CMDVT

int main(int argc, char *argv[]) {
  lapack_int info;
  DMat<IT, VT> A(DENSE_COLMAJOR);
  
  double start_time = read_timer();
  A.LoadNumPy(argv[1]);
  printf("[%10.4lf] Loaded.\n", read_timer() - start_time);
  info = LAPACKE_spotrf(LAPACK_ROW_MAJOR, 'U', A.rows_, A.values_, A.lda_);
  printf("[%10.4lf] info = %d\n", read_timer() - start_time, info);
  A.SaveNumPy("factor.npy");
  return 0;
}
