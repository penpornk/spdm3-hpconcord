// Included by distmatmul.h
#include <math.h>
#include <mpi.h>
#include "comm.h"
#include "dmat.h"
#include "spmat.h"
#include "timer.h"

namespace spdm3 {

template <class IT, class VT>
void innerABC_shiftc(DistSpMat<IT, VT> &A,
                   DistDMat<IT, VT> &B,
                   DistDMat<IT, VT> &C) {
  // Checks if #layers match.
  assert(A.layers() == B.layers());
  IT c = A.layers();

  // Constructs C's displs array.
  IT nteams = A.teams();
  IT rounds = nteams / c;
  IT row_id = (C.comm_[TEAM].id + C.comm_[LAYER].id) % c;
  IT *displs = new IT[rounds + 1];
  displs[0] = 0;
  for (int r = 1; r <= rounds; ++r) {
    displs[r] = displs[r-1] + A.row_counts_[row_id];
    row_id += c;
  }

  C.SetSize(A.rows(), B.cols());
  C.mat_.rows_ = displs[rounds];
  C.mat_.Fill(0.0);

  // Note that this is % nteams not % c like above.
  row_id = (C.comm_[TEAM].id + C.comm_[LAYER].id) % nteams;
  A.Shift(LAYER_COL, A.layer_id());
  for (int r = 0; r < rounds; ++r) {
    IT blk_idx = row_id / c;
    IT C_offset = displs[blk_idx] * C.llda();
    DMat<IT, VT> subC(A.lrows(), B.lcols(), C.llda(),
                      C.mat_.format_, C.mat_.values_ + C_offset);

    A.mat_.MultiplyAdd(B.mat_, subC);
    A.Shift(LAYER_COL, c);
    row_id = (row_id + c) % nteams;
  }
  delete [] displs;
}

template <class IT, class VT>
VT innerABC_shiftc_w_trace(DistSpMat<IT, VT> &A,
                           DistDMat<IT, VT> &B,
                           DistDMat<IT, VT> &C, Timer &T) {
  // Checks if #layers match.
  T.Start("InnerTrace");
  assert(A.layers() == B.layers());
  IT c = A.layers();

  // Constructs C's displs array.
  IT nteams = A.teams();
  IT rounds = nteams / c;
  IT row_id = (C.comm_[TEAM].id + C.comm_[LAYER].id) % c;
  IT *displs = new IT[rounds + 1];
  displs[0] = 0;
  for (int r = 1; r <= rounds; ++r) {
    displs[r] = displs[r-1] + A.row_counts_[row_id];
    row_id += c;
  }

  C.SetSize(A.rows(), B.cols());
  C.mat_.rows_ = displs[rounds];
  C.mat_.Fill(0.0);

  // Actual multiplicaton.
  // Note that this is % nteams not % c like above.
  VT trace = 0.0;
  row_id = (C.comm_[TEAM].id + C.comm_[LAYER].id) % nteams;
  T.Start("InnerTrace: Shift A");
  A.Shift(LAYER_COL, A.layer_id());
  T.Stop("InnerTrace: Shift A");
  for (int r = 0; r < rounds; ++r) {
    IT blk_idx = row_id / c;
    IT C_offset = displs[blk_idx] * C.llda();
    DMat<IT, VT> subC(A.lrows(), B.lcols(), C.llda(),
                      C.mat_.format_, C.mat_.values_ + C_offset);

    T.Start("InnerTrace: Matmul");
    A.mat_.MultiplyAdd(B.mat_, subC);
    T.Stop("InnerTrace: Matmul");

    // Accumulating trace(OSO).
    SpMat<IT, VT> subA;
    subA.Submatrix(A.mat_, 0, C.col_displs_[C.comm_[LAYER_ROW].rank],
                   A.lrows(), C.col_counts_[C.comm_[LAYER_ROW].rank]);
    subA.DotMultiplyI(subC);
    trace += subA.Reduce(SPDM3_SUM);

    T.Start("InnerTrace: Shift A");
    A.Shift(LAYER_COL, c);
    T.Stop("InnerTrace: Shift A");
    row_id = (row_id + c) % nteams;
  }
  delete [] displs;
  T.Stop("InnerTrace");

  // Returns partial trace to be summed up later.
  return trace;
}

template <class IT, class VT>
VT innerABC_shiftc_w_trace_piggyback(DistSpMat<IT, VT> &A,
                                     DistDMat<IT, VT> &B,
                                     DistDMat<IT, VT> &C, Timer &T) {
  // Checks if #layers match.
  T.Start("InnerTrace");
  assert(A.layers() == B.layers());
  IT c = A.layers();

  // Constructs C's displs array.
  IT nteams = A.teams();
  IT rounds = nteams / c;
  IT row_id = (C.comm_[TEAM].id + C.comm_[LAYER].id) % c;
  IT *displs = new IT[rounds + 1];
  displs[0] = 0;
  for (int r = 1; r <= rounds; ++r) {
    displs[r] = displs[r-1] + A.row_counts_[row_id];
    row_id += c;
  }

  C.SetSize(A.rows(), B.cols());
  C.mat_.rows_ = displs[rounds];
  C.mat_.Fill(0.0);

  // Actual multiplicaton.
  // Note that this is % nteams not % c like above.
  VT trace = 0.0;
  row_id = (C.comm_[TEAM].id + C.comm_[LAYER].id) % nteams;
  T.Start("InnerTrace: Shift A");
  A.Shift(LAYER_COL, A.layer_id());
  T.Stop("InnerTrace: Shift A");
  for (int r = 0; r < rounds; ++r) {
    IT blk_idx = row_id / c;
    IT C_offset = displs[blk_idx] * C.llda();
    DMat<IT, VT> subC(A.lrows(), B.lcols(), C.llda(),
                      C.mat_.format_, C.mat_.values_ + C_offset);

    T.Start("InnerTrace: Matmul");
    A.mat_.MultiplyAdd(B.mat_, subC);
    T.Stop("InnerTrace: Matmul");

    // Accumulating trace(OSO).
    SpMat<IT, VT> subA;
    subA.Submatrix(A.mat_, 0, C.col_displs_[C.comm_[LAYER_ROW].rank],
                   A.lrows(), C.col_counts_[C.comm_[LAYER_ROW].rank]);
    subA.DotMultiplyI(subC);
    trace += subA.Reduce(SPDM3_SUM);

    T.Start("InnerTrace: Shift A");
    A.Shift(LAYER_COL, c);
    T.Stop("InnerTrace: Shift A");
    row_id = (row_id + c) % nteams;
  }
  delete [] displs;
  T.Stop("InnerTrace");

  // Returns partial trace to be summed up later.
  return trace;
}

template <class IT, class VT>
void createTransposeComm_innerABC_shiftc(DistDMat<IT, VT> &C,
                                         TransposeInfo &info) {
// TODO(penpornk): Document this (the idea behind).
  IT c = C.layers();
  IT c_square = c * c;
  IT c_grid_id = C.rank() / c_square;
  IT rank_in_c_grid = C.rank() % c_square;
  IT col_in_c_grid = rank_in_c_grid / c;
  IT row_in_c_grid = (rank_in_c_grid + col_in_c_grid) % c;
  bool upper_triangular = (row_in_c_grid <= col_in_c_grid);

  IT comm_id = 0;
  if (upper_triangular)
    comm_id = row_in_c_grid * c + col_in_c_grid;
  else
    comm_id = col_in_c_grid * c + row_in_c_grid;

  IT blocks = C.teams() / c;
  IT rank_in_comm = c_grid_id + upper_triangular * blocks;  // All lower triangulars first then uppers.
  MPI_Comm comm_transpose;
  MPI_Comm_split(C.comm_[WORLD].comm, comm_id, rank_in_comm, &info.comm);

  // Transposes the blocks
  info.num_grids_in_1D = blocks;
  info.sendcounts = new IT[blocks * 2];
  info.senddispls = new IT[blocks * 2 + 1];
  IT row_id = (C.team_id() + C.layer_id()) % c;
  if (C.comm_[LAYER].id == 0) {
    for (int i = 0; i < blocks; ++i)
      info.sendcounts[i] = C.col_counts_[row_id + i * c] * C.lcols();
    gen_displs_from_counts(blocks, info.sendcounts, info.senddispls);
  } else {
    if (upper_triangular) {
      for (int i = 0; i < blocks; ++i)
        info.sendcounts[i] = C.col_counts_[C.team_id()] * C.col_counts_[row_id + i * c];
      fill_vec(info.sendcounts + blocks, blocks, 0);
    } else {
      fill_vec(info.sendcounts, blocks, 0);
      for (int i = 0; i < blocks; ++i) {
        info.sendcounts[blocks + i] =
            C.col_counts_[row_id + i * c] * C.col_counts_[C.team_id()];
      }
    }
    gen_displs_from_counts(blocks * 2, info.sendcounts, info.senddispls);
  }
}

template <class IT, class VT>
void innerABC_shiftc_transpose(DistDMat<IT, VT> &C,
                               DistDMat<IT, VT> &T, TransposeInfo &info, Timer &t) {
  t.Start("InnerTranspose");
  t.Start("InnerTranspose: Transpose");
  IT *recvcounts = info.sendcounts;
  IT *recvdispls = info.senddispls;
  MPI_Alltoallv(C.mat_.values_, info.sendcounts, info.senddispls, C.mpi_vt_,
                T.tmp_mat_.values_, recvcounts, recvdispls, C.mpi_vt_,
                info.comm);
  t.Stop("InnerTranspose: Transpose");

  // Memset the buffer.
  memset(T.mat_.values_, 0, C.mat_.buffer_size_ * sizeof(VT));  // TODO(penpornk): remove this.

  // Transposes the subblocks
  IT recvdispls_offset = 0;
  IT row_id = (C.team_id() + C.layer_id()) % C.layers();
  for (int i = 0; i < info.num_grids_in_1D; ++i) {
    DMat<IT, VT> in(C.col_counts_[C.team_id()], C.col_counts_[row_id + i * C.layers()], C.mat_.format_,
                    T.tmp_mat_.values_ + recvdispls[recvdispls_offset + i]);
    DMat<IT, VT> out(in.cols_, in.rows_, C.mat_.format_,
                     T.mat_.values_ + recvdispls[recvdispls_offset + i]);
    out.Transpose(in, false);
  }

  T.Stop("InnerTranspose");
}

template <class IT, class VT>
void innerABC_shiftc_transpose(DistDMat<IT, VT> &C,
                               DistDMat<IT, VT> &T, Timer &t) {
  t.Start("InnerTranspose");
  // Only supports 1D block column layout for now.
  assert(C.layout() == ONED_BLOCK_COLUMN);

  t.Start("InnerTranspose: Set up");
  T.CommonCopy(C);
  T.mat_.Allocate(C.lrows(), C.lcols());

  // TODO(penpornk): Document this (the idea behind).
  IT c = C.layers();
  IT c_square = c * c;
  IT c_grid_id = C.rank() / c_square;
  IT rank_in_c_grid = C.rank() % c_square;
  IT col_in_c_grid = rank_in_c_grid / c;
  IT row_in_c_grid = (rank_in_c_grid + col_in_c_grid) % c;
  bool upper_triangular = (row_in_c_grid <= col_in_c_grid);

  IT comm_id = 0;
  if (upper_triangular)
    comm_id = row_in_c_grid * c + col_in_c_grid;
  else
    comm_id = col_in_c_grid * c + row_in_c_grid;

  IT blocks = C.teams() / c;
  IT rank_in_comm = c_grid_id + upper_triangular * blocks;  // All lower triangulars first then uppers.
  MPI_Comm comm_transpose;
  MPI_Comm_split(C.comm_[WORLD].comm, comm_id, rank_in_comm, &comm_transpose);

  // Transposes the blocks
  IT *sendcounts = new IT[blocks * 2];
  IT *senddispls = new IT[blocks * 2 + 1];
  IT *recvcounts = sendcounts;
  IT *recvdispls = senddispls;
  IT recvdispls_offset = 0;
  IT row_id = (C.team_id() + C.layer_id()) % c;
  if (C.comm_[LAYER].id == 0) {
    for (int i = 0; i < blocks; ++i)
      sendcounts[i] = C.col_counts_[row_id + i * c] * C.lcols();
    gen_displs_from_counts(blocks, sendcounts, senddispls);
  } else {
    if (upper_triangular) {
      for (int i = 0; i < blocks; ++i)
        sendcounts[i] = C.col_counts_[C.team_id()] * C.col_counts_[row_id + i * c];
      fill_vec(sendcounts + blocks, blocks, 0);
    } else {
      recvdispls_offset = blocks;
      fill_vec(sendcounts, blocks, 0);
      for (int i = 0; i < blocks; ++i) {
        sendcounts[recvdispls_offset + i] =
            C.col_counts_[row_id + i * c] * C.col_counts_[C.team_id()];
      }
    }
    gen_displs_from_counts(blocks * 2, sendcounts, senddispls);
  }
  t.Stop("InnerTranspose: Set up");

  t.Start("InnerTranspose: Transpose");
  MPI_Alltoallv(C.mat_.values_, sendcounts, senddispls, C.mpi_vt_,
                T.mat_.values_, recvcounts, recvdispls, C.mpi_vt_,
                comm_transpose);
  t.Stop("InnerTranspose: Transpose");

  // Allocates the buffer for transposing subblocks
  VT *buffer = new VT[C.mat_.buffer_size_];
  memset(buffer, 0, C.mat_.buffer_size_ * sizeof(VT));  // TODO(penpornk): remove this.

  // Transposes the subblocks
  row_id = (C.team_id() + C.layer_id()) % c;
  for (int i = 0; i < blocks; ++i) {
    DMat<IT, VT> in(C.col_counts_[C.team_id()], C.col_counts_[row_id + i * c], C.mat_.format_,
                    T.mat_.values_ + recvdispls[recvdispls_offset + i]);
    DMat<IT, VT> out(in.cols_, in.rows_, C.mat_.format_,
                     buffer + recvdispls[recvdispls_offset + i]);
    out.Transpose(in, false);
  }

  // Makes transpose buffer the real buffer.
  std::swap(buffer, T.mat_.values_);

  // Deallocation.
  MPI_Comm_free(&comm_transpose);
  delete [] buffer;
  delete [] sendcounts;
  delete [] senddispls;

  t.Stop("InnerTranspose");
}

template <class IT, class VT>
VT innerABC_shift1(DistDMat<IT, VT> &A,
                   DistDMat<IT, VT> &B,
                   DistDMat<IT, VT> &C) {
  // Supports just c = 1 for now.
  assert(A.layers() == 1);

  // Checks if #layers match.
  assert(A.layers() == B.layers());
  IT c = A.layers();

  C.SetSize(A.rows(), B.cols());
  C.mat_.Fill(0.0);

  IT rounds = A.teams() / c;
  IT blk_row_offset = (C.comm_[TEAM].id + C.comm_[LAYER].id) % A.teams();
  // TODO(penpornk): Make initial (diagonal) shift for when c > 1.
  for (int r = 0; r < rounds; ++r) {
    IT C_offset = A.row_displs_[blk_row_offset] * C.llda();
    DMat<IT, VT> subC(A.lrows(), B.lcols(), C.llda(),
                      C.mat_.format_, C.mat_.values_ + C_offset);

    A.mat_.MultiplyAdd(B.mat_, subC);
    A.Shift(LAYER, 1);
    blk_row_offset = (blk_row_offset + 1) % A.teams();
  }
}

template <class IT, class VT>
void innerABC_bcast(DistDMat<IT, VT> &A,
                    DistDMat<IT, VT> &B,
                    DistDMat<IT, VT> &C,
                    Timer &T) {
  DMat<IT, VT> A_local;
  A_local.DeepCopy(A.mat_);

  // Checks if #layers match.
  assert(A.layers() == B.layers());
  IT layers = A.layers();
  IT lcols = B.lcols();

  C.SetSize(A.rows(), B.cols());
  C.mat_.Fill(0.0);

  IT rounds = A.teams() / layers;
  IT offset = A.layer_id() * rounds;
  IT C_base = A.row_displs_[offset] * C.llda();
  IT C_offset = 0;
  for (int r = 0; r < rounds; ++r) {
    IT row_id = offset + r;
    T.Start("InnerBcast: copy");
    if (row_id == A.team_id())
      A.mat_.DeepCopy(A_local);
    T.Stop("InnerBcast: copy");

    T.Start("InnerBcast: bcast");
    A.Bcast(LAYER, row_id);
    T.Stop("InnerBcast: bcast");
    DMat<IT, VT> subC(A.lrows(), lcols, C.llda(),
                      C.mat_.format_, C.mat_.values_ + C_base + C_offset);
    T.Start("InnerBcast: computation");
    A.mat_.MultiplyAdd(B.mat_, subC);
    T.Stop("InnerBcast: computation");
    C_offset += A.lrows() * C.llda();
  }

  // Gathers in-place all parts of the block column from all team members.
  int *counts = new int[layers];
  int *displs = new int[layers + 1];

  for (int layer_id = 0; layer_id < layers; ++layer_id) {
    counts[layer_id] = 0;
    for (int r = 0; r < rounds; ++r)
      counts[layer_id] += A.row_counts_[layer_id * rounds + r] * lcols;
  }
  gen_displs_from_counts(layers, counts, displs);
  assert(C_offset == counts[A.layer_id()]);
  T.Start("InnerBcast: allgatherv");
  MPI_Allgatherv(MPI_IN_PLACE, C_offset, C.mpi_vt_,
                 C.mat_.values_, counts, displs, C.mpi_vt_, C.comm_[TEAM].comm);
  T.Stop("InnerBcast: allgatherv");

  delete [] counts;
  delete [] displs;

  // Restores the content of A back.
  A.mat_.DeepCopy(A_local);
}

// Takes pruned matrix (in dense format, WeirdLayoutBlockColumn),
// converts it to sparse submatrices, then re-layout it to
// 1D block row.
template <class IT, class VT>
void innerABC_shiftc_reform_sparse_from_blockColumn(DistDMat<IT, VT> &D,
                                                    DistSpMat<IT, VT> &S) {
  //
  // Makes sparse submatrices with contiguous storage.
  //
  IT layers = D.layers();
  IT teams = D.teams();
  IT team_id = D.team_id();
  IT nblocks = teams / layers;
  SpMat<IT, VT> *locals = new SpMat<IT, VT>[nblocks];

  // headptrs size = #columns (before transpose) + nblocks.
  // indices and values size = #nnz.
  LT nnz = D.mat_.nnz();
  IT lcols = D.lcols();
  LT *headptrs = new LT[(lcols + 1) * nblocks];  // TODO(penpornk): D still has way too many rows. Cut them off.
  LT *indices = new LT[nnz];
  VT *values = new VT[nnz];

  // Assigns SpMats to buffers and converts dense matrix to sparse.
  DMat<IT, VT> subd;
  IT row_offset = 0, col_offset = 0, nnz_offset = 0;
  IT row_id = (team_id + D.layer_id()) % layers;
  for (int i = 0; i < nblocks; ++i) {
    IT lrows = D.col_counts_[row_id];
    subd.ShallowSubDMat(D.mat_, row_offset, 0, lrows, lcols);
    DMat<IT, VT> transpose;
    transpose.Transpose(subd);

    IT lnnz = transpose.nnz();
    // TODO(penpornk): handle CSC?
    locals[i].PointersToSpMat(lcols, lrows, lnnz, SPARSE_CSR, 0,
                              headptrs + col_offset + i, indices + nnz_offset,
                              values + nnz_offset, false);
    locals[i].Convert(transpose, false);

    row_id += layers;
    row_offset += lrows;
    col_offset += lcols;
    nnz_offset += lnnz;
  }

  // Allgathers.
  int sendcount = col_offset + nblocks;
  int multi_head_length = sendcount * layers;
  LT *multi_headptrs = new LT[multi_head_length];
  MPI_Allgather(headptrs, sendcount, D.mpi_lt_,
                multi_headptrs, sendcount, D.mpi_lt_, D.comm_[TEAM].comm);

  int *val_counts = new int[layers];
  int *val_displs = new int[layers + 1];
  // TODO(penpornk): This can be calculated locally by going through all headptrs of all subblocks.
  MPI_Allgather(&nnz_offset, 1, MPI_INT, val_counts, 1, MPI_INT, D.comm_[TEAM].comm);
  gen_displs_from_counts(layers, val_counts, val_displs);

  int multi_val_length = val_displs[layers];
  LT *multi_indices = new LT[multi_val_length];
  VT *multi_values = new VT[multi_val_length];
  MPI_Allgatherv(indices, nnz_offset, D.mpi_lt_,
                 multi_indices, val_counts, val_displs, D.mpi_lt_, D.comm_[TEAM].comm);
  MPI_Allgatherv(values, nnz_offset, D.mpi_vt_,
                 multi_values, val_counts, val_displs, D.mpi_vt_, D.comm_[TEAM].comm);

  // Makes SpMat pointers, pointing in interleaving order.
  // Preserves head_displs and val_displs just in case I'll need them later.
  SpMat<IT, VT> *all = new SpMat<IT, VT>[teams];
  int *head_offset = new int[layers];
  int *val_offset = new int[layers];
  for (int i = 0; i < layers; ++i)
    head_offset[i] = sendcount * i;
  memcpy(val_offset, val_displs, layers * sizeof(int));

  IT srows = lcols;
  for (int i = 0; i < teams; ++i) {
    IT layer_id = (i + layers - (team_id % layers)) % layers;
    IT scols = D.col_counts_[i];
    IT snnz = multi_headptrs[head_offset[layer_id] + srows];

    all[i].PointersToSpMat(srows, scols, snnz, SPARSE_CSR, 0,
                           multi_headptrs + head_offset[layer_id],
                           multi_indices + val_offset[layer_id],
                           multi_values + val_offset[layer_id], false);

    head_offset[layer_id] += srows + 1;
    val_offset[layer_id] += snnz;
  }

  // Concatenates.
  S.SetSize(D.rows(), D.cols());
  S.mat_.Concatenate(all, 1, teams, ONED_BLOCK_COLUMN);
  S.ResetCounters();
}

// Takes transposed, pruned matrix (in dense format, WeirdLayoutBlockRow),
// converts it to sparse submatrices
template <class IT, class VT>
void innerABC_shiftc_reform_sparse_from_blockRow(DistDMat<IT, VT> &D,
                                                 DistSpMat<IT, VT> &S, Timer &T) {
  T.Start("InnerReform");

  //
  // Makes sparse submatrices with contiguous storage.
  //
  IT layers = D.layers();
  IT teams = D.teams();
  IT team_id = D.team_id();
  IT nblocks = teams / layers;
  SpMat<IT, VT> *locals = new SpMat<IT, VT>[nblocks];

  // headptrs size = #columns (before transpose) + nblocks.
  // indices and values size = #nnz.
  LT nnz = D.mat_.nnz();
  IT lrows = D.lrows();
  LT *headptrs = new LT[(lrows + 1) * nblocks];  // TODO(penpornk): D still has way too many rows. Cut them off.
  LT *indices = new LT[nnz];
  VT *values = new VT[nnz];

  // Assigns SpMats to buffers and converts dense matrix to sparse.
  DMat<IT, VT> subd;
  IT row_offset = 0, col_offset = 0, nnz_offset = 0;
  IT col_id = (team_id + D.layer_id()) % layers;
  for (int i = 0; i < nblocks; ++i) {
    IT lcols = D.row_counts_[col_id];
    subd.ShallowSubDMat(D.mat_, 0, col_offset, lrows, lcols);

    IT lnnz = subd.nnz();
    // TODO(penpornk): handle CSC?
    locals[i].PointersToSpMat(lrows, lcols, lnnz, SPARSE_CSR, 0,
                              headptrs + row_offset + i, indices + nnz_offset,
                              values + nnz_offset, false);
    locals[i].Convert(subd, false);

    col_id += layers;
    row_offset += lrows;
    col_offset += lcols;
    nnz_offset += lnnz;
  }

  // Allgathers.
  int sendcount = row_offset + nblocks;
  int multi_head_length = sendcount * layers;
  LT *multi_headptrs = new LT[multi_head_length];
  MPI_Allgather(headptrs, sendcount, D.mpi_lt_,
                multi_headptrs, sendcount, D.mpi_lt_, D.comm_[TEAM].comm);

  int *val_counts = new int[layers];
  int *val_displs = new int[layers + 1];
  // TODO(penpornk): This can be calculated locally by going through all headptrs of all subblocks.
  MPI_Allgather(&nnz_offset, 1, MPI_INT, val_counts, 1, MPI_INT, D.comm_[TEAM].comm);
  gen_displs_from_counts(layers, val_counts, val_displs);

  int multi_val_length = val_displs[layers];
  LT *multi_indices = new LT[multi_val_length];
  VT *multi_values = new VT[multi_val_length];
  MPI_Allgatherv(indices, nnz_offset, D.mpi_lt_,
                 multi_indices, val_counts, val_displs, D.mpi_lt_, D.comm_[TEAM].comm);
  MPI_Allgatherv(values, nnz_offset, D.mpi_vt_,
                 multi_values, val_counts, val_displs, D.mpi_vt_, D.comm_[TEAM].comm);

  // Makes SpMat pointers, pointing in interleaving order.
  // Preserves head_displs and val_displs just in case I'll need them later.
  SpMat<IT, VT> *all = new SpMat<IT, VT>[teams];
  int *head_offset = new int[layers];
  int *val_offset = new int[layers];
  for (int i = 0; i < layers; ++i)
    head_offset[i] = sendcount * i;
  memcpy(val_offset, val_displs, layers * sizeof(int));

  IT srows = lrows;
  for (int i = 0; i < teams; ++i) {
    IT layer_id = (i + layers - (team_id % layers)) % layers;
    IT scols = D.row_counts_[i];
    IT snnz = multi_headptrs[head_offset[layer_id] + srows];

    all[i].PointersToSpMat(srows, scols, snnz, SPARSE_CSR, 0,
                           multi_headptrs + head_offset[layer_id],
                           multi_indices + val_offset[layer_id],
                           multi_values + val_offset[layer_id], false);

    head_offset[layer_id] += srows + 1;
    val_offset[layer_id] += snnz;
  }

  // Concatenates.
  S.SetSize(D.rows(), D.cols());
  S.mat_.Concatenate(all, 1, teams, ONED_BLOCK_COLUMN);
  S.ResetCounters();

  T.Stop("InnerReform");
}

//
// Computes sparse (S) x dense (D)  = dense (C).
//
template <class IT, class VT>
void allblkrow_shift1(DistSpMat<IT, VT> &S,
                      DistDMat<IT, VT> &D,
                      DistDMat<IT, VT> &C) {
  // Must all be 1D block row layout.
  assert(S.layout() == ONED_BLOCK_ROW &&
         D.layout() == ONED_BLOCK_ROW &&
         C.layout() == ONED_BLOCK_ROW);

  // C must have the same number of layers as S.
  assert(C.layers() == S.layers());
  C.SetSize(S.rows(), D.cols());
  C.mat_.Fill(0.0);

  // Finds offset.
  int layer_id = S.layer_id() % D.layers();  // Or vise versa: D.layer_id() % S.layers().
  int rounds = D.teams() / S.layers();
  int shift = std::max(1, S.layers() / D.layers());
  int blks_per_layer = shift * rounds;
  int start_offset = layer_id * blks_per_layer;
  int lrows = S.lrows();

  D.Shift(LAYER_COL, start_offset);
  for (int i = 0; i < rounds; ++i) {
    int row_id  = D.owner_rank_[LAYER_COL];
    SpMat<IT, VT> subS;
    subS.Submatrix(S.mat_, 0, D.row_displs_[row_id], lrows, D.row_counts_[row_id]);
    subS.MultiplyAdd(D.mat_, C.mat_);

    D.Shift(LAYER_COL, shift);
  }
  C.Allreduce(TEAM);
}

//
// Computes dense (D) x dense (D)  = dense (C).
//
template <class IT, class VT>
void allblkrow_shiftc(DistDMat<IT, VT> &S,
                      DistDMat<IT, VT> &D,
                      DistDMat<IT, VT> &C, Timer &T) {
  T.Start("ABR");
  // Must all be 1D block row layout.
  assert(S.layout() == ONED_BLOCK_ROW &&
         D.layout() == ONED_BLOCK_ROW &&
         C.layout() == ONED_BLOCK_ROW);

  // C must have the same number of layers as S.
  assert(C.layers() == S.layers());
  C.SetSize(S.rows(), D.cols());
  T.Start("ABR: fill");
  C.mat_.Fill(0.0);
  T.Stop("ABR: fill");

  // Finds offset.
  int layer_id = S.layer_id() % D.layers();  // Or vise versa: D.layer_id() % S.layers().
  int rounds = D.teams() / S.layers();
  int shift_offset = std::max(1, S.layers() / D.layers());
  int start_offset = layer_id * shift_offset;
  int lrows = S.lrows();

  T.Start("ABR: shift");
  D.Shift(LAYER_COL, start_offset);
  T.Stop("ABR: shift");
  for (int i = 0; i < rounds; ++i) {
    int row_id  = D.owner_rank_[LAYER_COL];
    DMat<IT, VT> subS;
    subS.ShallowSubDMat(S.mat_, 0, D.row_displs_[row_id], lrows, D.row_counts_[row_id]);
    T.Start("ABR: matmul");
    subS.MultiplyAdd(D.mat_, C.mat_);
    T.Stop("ABR: matmul");

    T.Start("ABR: shift");
    D.Shift(LAYER_COL, S.layers());
    T.Stop("ABR: shift");
  }
  T.Start("ABR: allreduce");
  C.Allreduce(TEAM);
  T.Stop("ABR: allreduce");
  T.Stop("ABR");
}

//
// Computes sparse (S) x dense (D)  = dense (C).
//
template <class IT, class VT>
void allblkrow_shiftc(DistSpMat<IT, VT> &S,
                      DistDMat<IT, VT> &D,
                      DistDMat<IT, VT> &C) {
  // Must all be 1D block row layout.
  assert(S.layout() == ONED_BLOCK_ROW &&
         D.layout() == ONED_BLOCK_ROW &&
         C.layout() == ONED_BLOCK_ROW);

  // C must have the same number of layers as S.
  assert(C.layers() == S.layers());
  C.SetSize(S.rows(), D.cols());
  C.mat_.Fill(0.0);

  // Finds offset.
  int layer_id = S.layer_id() % D.layers();  // Or vise versa: D.layer_id() % S.layers().
  int rounds = D.teams() / S.layers();
  int shift_offset = std::max(1, S.layers() / D.layers());
  int start_offset = layer_id * shift_offset;
  int lrows = S.lrows();

  D.Shift(LAYER_COL, start_offset);
  for (int i = 0; i < rounds; ++i) {
    int row_id  = D.owner_rank_[LAYER_COL];
    SpMat<IT, VT> subS;
    subS.Submatrix(S.mat_, 0, D.row_displs_[row_id], lrows, D.row_counts_[row_id]);
    subS.MultiplyAdd(D.mat_, C.mat_);

    D.Shift(LAYER_COL, S.layers());
  }
  C.Allreduce(TEAM);
}


//
// Computes sparse (S) x dense (D)  = dense (C).
//
template <class IT, class VT>
void allblkrow_shiftc(SpMat<IT, VT> *&M,
                      DistDMat<IT, VT> &D,
                      DistDMat<IT, VT> &C, Timer &T) {
  T.Start("Allblkrow");
  // Must all be 1D block row layout.
  assert(D.layout() == ONED_BLOCK_ROW &&
         C.layout() == ONED_BLOCK_ROW);

  // C must have the same number of layers as S.
  // Assumes C's size is already set.
  // C.SetSize(S.rows(), D.cols());
  T.Start("Allblkrow: fill");
  C.mat_.Fill(0.0);
  T.Stop("Allblkrow: fill");

  // Finds offset.
  T.Start("Allblkrow: prep");
  int layers = C.layers();
  int layer_id = D.layer_id() % layers;
  int rounds = D.teams() / layers;
  int shift_offset = std::max(1, layers / D.layers());
  int start_offset = layer_id * shift_offset;
  int lrows = C.lrows();
  T.Stop("Allblkrow: prep");

  T.Start("Allblkrow: shift");
  D.Shift(LAYER_COL, start_offset - D.distance_shifted_[LAYER_COL]);
  T.Stop("Allblkrow: shift");
  int idx = D.owner_rank_[LAYER_COL] / layers;
  for (int i = 0; i < rounds; ++i) {
    T.Start("Allblkrow: matmul");
    M[idx].MultiplyAdd(D.mat_, C.mat_);
    T.Stop("Allblkrow: matmul");

    T.Start("Allblkrow: shift");
    D.Shift(LAYER_COL, layers);
    T.Stop("Allblkrow: shift");
    idx = (idx + 1) % rounds;
  }
  T.Start("Allblkrow: allreduce");
  C.Allreduce(TEAM);
  T.Stop("Allblkrow: allreduce");
  T.Stop("Allblkrow");
}

template <class IT, class VT>
void inner_shiftc_rotatingB(DistDMat<IT, VT> &A,
                            DistDMat<IT, VT> &B,
                            DistDMat<IT, VT> &C, Timer &T) {
  T.Start("Inner");
  // Must all be 1D block row layout.
  assert(A.layout() == ONED_BLOCK_ROW &&
         B.layout() == ONED_BLOCK_COLUMN &&
         C.layout() == ONED_BLOCK_ROW);

  // C must have the same number of layers as S.
  assert(C.layers() == A.layers());

  // Constructs C's displs array.
  T.Start("Inner: prepare");
  IT c = C.layers();
  IT nteams = B.teams();
  IT rounds = nteams / c;
  IT blk_col_size = std::max(1, c / B.layers());
  IT col_id = (B.team_id() + A.layer_id() * blk_col_size) % c;
  IT *displs = new IT[rounds + 1];
  displs[0] = 0;
  for (int r = 1; r <= rounds; ++r) {
    displs[r] = displs[r-1] + B.col_counts_[col_id];
    col_id += c;
  }
  T.Stop("Inner: prepare");

  // Sets up C.
  T.Start("Inner: fill");
  C.SetSize(A.rows(), displs[rounds]);
  C.mat_.Fill(0.0);
  T.Stop("Inner: fill");

  // Finds offset.
  int layer_id = A.layer_id() % B.layers();  // Or vise versa: D.layer_id() % S.layers().
  int start_offset = layer_id * blk_col_size;
  int lrows = A.lrows();

  T.Start("Inner: shift");
  B.Shift(LAYER_ROW, start_offset - B.distance_shifted_[LAYER_ROW]);
  T.Stop("Inner: shift");
  for (int i = 0; i < rounds; ++i) {
    T.Start("Inner: matmul");
    int col_id  = B.owner_rank_[LAYER_ROW];
    IT blk_idx = col_id / C.layers();
    IT C_offset = displs[blk_idx] * C.lrows();
    DMat<IT, VT> subC(A.lrows(), B.lcols(), B.lcols(),
                      C.mat_.format_, C.mat_.values_ + C_offset);
    A.mat_.MultiplyAdd(B.mat_, subC);
    T.Stop("Inner: matmul");

    T.Start("Inner: shift");
    B.Shift(LAYER_ROW, A.layers());
    T.Stop("Inner: shift");
  }

  delete [] displs;
  T.Stop("Inner");
}

template <class IT, class VT>
void inner_shiftc_rotatingB_async(DistDMat<IT, VT> &A,
                                  DistDMat<IT, VT> &B,
                                  DistDMat<IT, VT> &C, Timer &T) {
  T.Start("Inner");
  // Must all be 1D block row layout.
  assert(A.layout() == ONED_BLOCK_ROW &&
         B.layout() == ONED_BLOCK_COLUMN &&
         C.layout() == ONED_BLOCK_ROW);

  // C must have the same number of layers as S.
  assert(C.layers() == A.layers());

  // Constructs C's displs array.
  T.Start("Inner: prepare");
  IT c = C.layers();
  IT nteams = B.teams();
  IT rounds = nteams / c;
  IT blk_col_size = std::max(1, c / B.layers());
  IT col_id = (B.team_id() + A.layer_id() * blk_col_size) % c;
  IT *displs = new IT[rounds + 1];
  displs[0] = 0;
  for (int r = 1; r <= rounds; ++r) {
    displs[r] = displs[r-1] + B.col_counts_[col_id];
    col_id += c;
  }
  T.Stop("Inner: prepare");

  // Sets up C.
  T.Start("Inner: fill");
  C.SetSize(A.rows(), displs[rounds]);
  C.mat_.Fill(0.0);
  T.Stop("Inner: fill");

  // Finds offset.
  int layer_id = A.layer_id() % B.layers();  // Or vise versa: D.layer_id() % S.layers().
  int start_offset = layer_id * blk_col_size;
  int lrows = A.lrows();

  T.Start("Inner: diag shift");
  B.Shift(LAYER_ROW, start_offset - B.distance_shifted_[LAYER_ROW]);
  T.Stop("Inner: diag shift");
  for (int i = 0; i < rounds; ++i) {
    T.Start("Inner: shiftWait");
    B.ShiftWait();
    T.Stop("Inner: shiftWait");
    T.Start("Inner: shiftAsync");
    B.ShiftAsync(LAYER_ROW, A.layers());
    T.Stop("Inner: shiftAsync");

    T.Start("Inner: matmul");
    int col_id  = B.owner_rank_[LAYER_ROW];
    IT blk_idx = col_id / C.layers();
    IT C_offset = displs[blk_idx] * C.lrows();
    DMat<IT, VT> subC(A.lrows(), B.lcols(), B.lcols(),
                      C.mat_.format_, C.mat_.values_ + C_offset);
    A.mat_.MultiplyAdd(B.mat_, subC);
    T.Stop("Inner: matmul");
  }

  delete [] displs;
  T.Stop("Inner");
}


template <class IT, class VT>
DMat<IT, VT> *InnerShiftCRotatingBShallowDMats(DistDMat<IT, VT> &C,
                                               DistDMat<IT, VT> &B) {
  // Must all be 1D block row layout.
  assert(C.layout() == ONED_BLOCK_ROW &&
         B.layout() == ONED_BLOCK_COLUMN);

  // Constructs C's displs array.
  IT c = C.layers();
  IT nteams = B.teams();
  IT rounds = nteams / c;
  IT blk_col_size = std::max(1, c / B.layers());
  IT col_id = (B.team_id() + C.layer_id() * blk_col_size) % c;
  IT colcount = 0;
  for (int r = 1; r <= rounds; ++r) {
    colcount += B.col_counts_[col_id];
    col_id += c;
  }

  int C_offset = 0;
  DMat<IT, VT> *subC = new DMat<IT, VT>[rounds];
  col_id = (B.team_id() + C.layer_id() * blk_col_size) % c;
  for (int r = 0; r < rounds; ++r) {
    int rows = C.lrows();
    int cols = B.col_counts_[col_id];
    subC[r].PointerToDMat(rows, cols, cols,
                          C.mat_.format_, C.mat_.values_ + C_offset, false);
    C_offset += rows * cols;
    col_id += c;
  }
  return subC;
}

template <class IT, class VT>
void createTransposeInfo_inner_shiftc_rotatingB(DistDMat<IT, VT> &A,
                                                DistDMat<IT, VT> &B,
                                                TransposeInfo &info,
                                                Timer &T) {
  T.Start("CreateTransposeInfo");
  info.rank = A.rank();
  info.team_row_id = A.team_id();

  info.c_f = A.layers();
  info.c_r = B.layers();
  info.row_teams = A.teams();
  info.col_teams = B.teams();
  info.ranks_per_grid = info.c_f * info.c_r;
  info.num_grids_in_1D = info.col_teams / info.c_f;

  int c_min, c_max;
  if (info.c_f > info.c_r) {
    info.c_ratio = info.c_f / info.c_r;
    info.col_teams_per_blk = info.c_ratio;
    info.row_teams_per_blk = 1;
    info.starting_team_col_id =
        (B.team_id() + B.layer_id() * info.c_ratio) % info.c_f;
    c_min = info.c_r;
    c_max = info.c_f;
  } else {
    info.c_ratio = info.c_r / info.c_f;
    info.row_teams_per_blk = info.c_ratio;
    info.col_teams_per_blk = 1;
    info.starting_team_col_id =
        (B.team_id() + A.layer_id()) % info.c_f;
    c_min = info.c_f;
    c_max = info.c_r;
  }
  info.comm_size = info.num_grids_in_1D * info.c_ratio;
  info.blks_per_grid = info.c_f / info.col_teams_per_blk;

  int rank_in_grid = info.rank % info.ranks_per_grid;
  int layer_min = std::min(A.layer_id(), B.layer_id());
  int blk_row_id = rank_in_grid / c_max;
  int blk_col_id = (blk_row_id + layer_min) % info.blks_per_grid;
  bool upper_triangular = (blk_row_id < blk_col_id);
  bool diagonal = (blk_row_id == blk_col_id);

  // Allocates counts.
  int max_comm_size = info.comm_size * 2;
  info.rowcounts = new int[info.comm_size];
  info.colcounts = new int[info.comm_size];
  info.sendcounts = new int[max_comm_size];
  info.senddispls = new int[max_comm_size + 1];
  info.ldas = new int[info.num_grids_in_1D];

  // Creates new communicator for transpose.
  int comm_id, rank_in_comm;
  if (upper_triangular || diagonal) {
    comm_id = blk_row_id * c_max + blk_col_id;
    rank_in_comm = info.rank + A.comm_[WORLD].size;
    info.sendcounts_short = info.sendcounts;
    info.senddispls_short = info.senddispls;
    fill_vec(info.sendcounts + info.comm_size, info.comm_size, 0);
  } else {
    comm_id = blk_col_id * c_max + blk_row_id;
    rank_in_comm = info.rank;
    fill_vec(info.sendcounts, info.comm_size, 0);
    info.sendcounts_short = info.sendcounts + info.comm_size;
    info.senddispls_short = info.senddispls + info.comm_size;
  }
  MPI_Comm_split(A.comm_[WORLD].comm, comm_id, rank_in_comm, &info.comm);

  // Calculates rowcounts, colcounts, ldas, and sendcounts.
  int idx = 0;
  int team_col_id = info.starting_team_col_id;
  if (info.c_f > info.c_r) {
    for (int g = 0; g < info.num_grids_in_1D; ++g) {
      info.ldas[g] = B.col_counts_[team_col_id];
      for (int i = 0; i < info.c_ratio; ++i) {
        info.rowcounts[idx] = B.col_counts_[A.team_id() * info.c_ratio + i];
        info.colcounts[idx] = B.col_counts_[team_col_id];
        info.sendcounts_short[idx] = info.rowcounts[idx] * info.colcounts[idx];
        ++idx;
      }
      team_col_id += info.c_f;
    }
  } else {
    for (int g = 0; g < info.num_grids_in_1D; ++g) {
      info.ldas[g] = B.col_counts_[team_col_id];
      for (int i = 0; i < info.c_ratio; ++i) {
        info.rowcounts[idx] = A.lrows();
        info.colcounts[idx] = A.row_counts_[team_col_id * info.c_ratio + i];
        info.sendcounts_short[idx] = info.rowcounts[idx] * info.colcounts[idx];
        ++idx;
      }
      team_col_id += info.c_f;
    }
  }
  gen_displs_from_counts(max_comm_size, info.sendcounts, info.senddispls);
  T.Stop("CreateTransposeInfo");
}

template <class IT, class VT>
void inner_shiftc_rotatingB_transpose(DistDMat<IT, VT> &C, DistDMat<IT, VT> &CT,
                                      TransposeInfo &info, Timer &T) {
  T.Start("InnerTranspose");
  // Only supports 1D block column layout for now.
  assert(C.layout() == ONED_BLOCK_ROW);

  T.Start("InnerTranspose: prep CT");
  CT.CommonCopy(C);
  CT.mat_.Allocate(C.lrows(), C.lcols());
  T.Stop("InnerTranspose: prep CT");

  // Fills sendbuffer.
  VT *sendbuffer;
  DMat<IT, VT> outer, s, temp;
  T.Start("InnerTranspose: sendbuf");
  if (info.c_r > info.c_f) {
    temp.Allocate(C.lrows(), C.lcols());
    int idx = 0, blk_offset = 0, col_offset = 0;
    for (int g = 0; g < info.num_grids_in_1D; ++g) {
      DMat<IT, VT> outer(C.lrows(), info.ldas[g], C.mat_.format_, C.mat_.values_ + blk_offset);
      col_offset = 0;
      for (int i = 0; i < info.c_ratio; ++i) {
        s.ShallowSubDMat(outer, 0, col_offset, C.lrows(), info.colcounts[idx]);
        DMat<IT, VT> out(s.rows_, s.cols_, s.format_, temp.values_ + info.senddispls_short[idx]);
        out.SetSubDMat(s, 0, 0);
        col_offset += info.colcounts[idx];
        ++idx;
      }
      blk_offset += outer.dim1() * outer.lda_;
    }
    sendbuffer = temp.values_;
  } else {
    sendbuffer = C.mat_.values_;
  }
  T.Stop("InnerTranspose: sendbuf");

  // Transposes the blocks.
  int *recvcounts = info.sendcounts;
  int *recvdispls = info.senddispls;
  int *recvdispls_short = info.senddispls_short;
  T.Start("InnerTranspose: alltoallv");
  MPI_Alltoallv(sendbuffer, info.sendcounts, info.senddispls, C.mpi_vt_,
                CT.mat_.values_, recvcounts, recvdispls, CT.mpi_vt_,
                info.comm);
  T.Stop("InnerTranspose: alltoallv");

  // Allocates the buffer for transposing subblocks
  VT *buffer = new VT[C.mat_.buffer_size_];

  // Transposes the subblocks.
  T.Start("InnerTranspose: lxpose");
  int idx = 0, col_offset = 0, blk_offset = 0;
  for (int g = 0; g < info.num_grids_in_1D; ++g) {
    col_offset = 0;
    int out_lda = info.ldas[g];
    for (int i = 0; i < info.c_ratio; ++i) {
      int in_rows, in_cols; //, out_lda;
      in_rows = info.colcounts[idx];
      in_cols = info.rowcounts[idx];
      DMat<IT, VT> in(in_rows, in_cols, C.mat_.format_,
                      CT.mat_.values_ + recvdispls_short[idx]);
      DMat<IT, VT> out(in.cols_, in.rows_, out_lda, C.mat_.format_,
                       buffer + blk_offset + col_offset);

      out.Transpose(in, false);
      if (info.c_r > info.c_f) {
        col_offset += out.cols_;
      } else {
        col_offset += out.dim1() * out.lda_;
      }
      ++idx;
    }
    if (info.c_r > info.c_f) {
      blk_offset += C.lrows() * out_lda;
    } else {
      blk_offset += col_offset;
    }
  }
  T.Stop("InnerTranspose: lxpose");

  // Makes transpose buffer the real buffer.
  std::swap(buffer, CT.mat_.values_);

  // Deallocation.
  delete [] buffer;

  T.Stop("InnerTranspose");
}

template <class IT, class VT>
SpMat<IT, VT> *GetWeirdLayoutRotatingBSpMats(DistSpMat<IT, VT> &X, DistDMat<IT, VT> &B) {
  // Sanity check.
  assert(X.rows() == X.cols());  // Must be square.
  assert(X.layout() == ONED_BLOCK_ROW);
  assert(B.layout() == ONED_BLOCK_COLUMN);

  // Find out number of rounds.
  int local_cols = 0;
  int c = X.layers();
  int nteams = B.teams();
  int nblocks = nteams / c;
  int blk_col_size = std::max(1, c / B.layers());
  int layer_id = std::min(X.layer_id(), B.layer_id());
  int starting_blk_idx = (B.team_id() + layer_id * blk_col_size) % c;

  // Allocates SpMat array.
  SpMat<IT, VT> *M = new SpMat<IT, VT>[nblocks];
  int blk_idx = starting_blk_idx;
  for (int i = 0; i < nblocks; ++i) {
    M[i].Submatrix(X.mat_, 0, B.col_displs_[blk_idx],
                   X.mat_.rows_, B.col_counts_[blk_idx]);
    blk_idx += c;
  }
  return M;
}

template <class IT, class VT>
SpMat<IT, VT> *UnWeirdLayoutRotatingBSpMats(SpMat<IT, VT>* M, DistSpMat<IT, VT> &X, DistDMat<IT, VT> &B) {
  // Sanity check.
  assert(X.rows() == X.cols());  // Must be square.
  assert(X.layout() == ONED_BLOCK_ROW);
  assert(B.layout() == ONED_BLOCK_COLUMN);

  // Find out number of rounds.
  int local_cols = 0;
  int c = X.layers();
  int nteams = B.teams();
  int nblocks = nteams / c;
  int blk_col_size = std::max(1, c / B.layers());
  int layer_id = std::min(X.layer_id(), B.layer_id());
  int starting_blk_idx = (B.team_id() + layer_id * blk_col_size) % c;

  SpMat<IT, VT> *U = new SpMat<IT, VT>[nblocks];
  int blk_idx = starting_blk_idx;
  for (int i = 0; i < nblocks; ++i) {
    U[blk_idx].ShallowCopy(M[i]);
    blk_idx += c;
  }
  return U;
}

template <class IT, class VT>
DMat<IT, VT> *GetWeirdLayoutRotatingBDMats(DistDMat<IT, VT> &X, DistDMat<IT, VT> &B) {
  // Sanity check.
  assert(X.rows() == X.cols());  // Must be square.
  assert(X.layout() == ONED_BLOCK_ROW);
  assert(B.layout() == ONED_BLOCK_COLUMN);

  // Find out number of rounds.
  int local_cols = 0;
  int c = X.layers();
  int nteams = B.teams();
  int nblocks = nteams / c;
  int blk_col_size = std::max(1, c / B.layers());
  int layer_id = std::min(X.layer_id(), B.layer_id());
  int starting_blk_idx = (B.team_id() + layer_id * blk_col_size) % c;

  // Allocates DMat array.
  DMat<IT, VT> *M = new DMat<IT, VT>[nblocks];
  int blk_idx = starting_blk_idx;
  for (int i = 0; i < nblocks; ++i) {
    M[i].SubDMat(X.mat_, 0, B.col_displs_[blk_idx],
                 X.mat_.rows_, B.col_counts_[blk_idx]);
    blk_idx += c;
  }
  return M;
}

template <class IT, class VT>
bool GetDiagonalWeirdLayoutRotatingBOffsets(TransposeInfo &info, int &idx,
                                            int &row_offset, int &col_offset) {
  // Finds the block to operate on.
  int row_id = info.team_row_id;
  int rank = info.rank;
  idx = row_id / info.c_r;

  // Finds row and column offset.
  row_offset = col_offset = 0;
  if (info.c_f > info.c_r) {
    int rank_in_row = rank % info.c_f;
    if (rank_in_row % info.c_r > 0)
      return false;
    int rank_in_block = rank_in_row / info.c_r;
    for (int i = 0; i < rank_in_block; ++i) {
      row_offset += info.rowcounts[idx * info.c_ratio + i];
    }
  } else {
    int rank_in_row = rank % info.c_r;
    if (rank_in_row % info.c_f > 0)
      return false;
    int rank_in_block = rank_in_row / info.c_f;
    for (int i = 0; i < rank_in_block; ++i) {
      col_offset += info.colcounts[idx * info.c_ratio + i];
    }
  }
  return true;
}

template <class IT, class VT>
void OpDiagonalWeirdLayoutRotatingB(std::function<VT(VT, VT)> fn,
                                    DMat<IT, VT> *D, SpMat<IT, VT> *S,
                                    TransposeInfo &info) {
  int idx = 0, row_offset = 0, col_offset = 0;
  if (!GetDiagonalWeirdLayoutRotatingBOffsets<IT, VT>(
           info, idx, row_offset, col_offset))
    return;

  // Calculation.
  #pragma omp parallel for schedule(guided)
  for (int i = row_offset; i < S[idx].rows_; ++i) {
    for (int k = S[idx].headptrs_[i]; k < S[idx].headptrs_[i+1]; ++k) {
      int j = S[idx].indices_[k];
      if (i - row_offset == j - col_offset && i - row_offset >= 0) {
        int d_idx = i * D[idx].lda_ + j;
        D[idx].values_[d_idx] =
          fn(D[idx].values_[d_idx], S[idx].values_[k]);
      }
    }
  }
}

template <class IT, class VT>
VT ReduceDiagonalWeirdLayoutRotatingB(std::function<VT(VT, VT)> fn,
                                      VT init_accu_value,
                                      SpMat<IT, VT> *S,
                                      TransposeInfo &info) {
  int idx = 0, row_offset = 0, col_offset = 0;
  if (!GetDiagonalWeirdLayoutRotatingBOffsets<IT, VT>(
           info, idx, row_offset, col_offset))
    return init_accu_value;

  // Calculation.
  // TODO(penpornk): confirm with Sang that he wants natural log.
  VT res = init_accu_value;
  #pragma omp parallel for schedule(guided) reduction(+ : res)
  for (int i = row_offset; i < S[idx].rows_; ++i) {
    for (int k = S[idx].headptrs_[i]; k < S[idx].headptrs_[i+1]; ++k) {
      int j = S[idx].indices_[k];
      if (i - row_offset == j - col_offset && i - row_offset >= 0) {
        res = fn(res, S[idx].values_[k]);
      }
    }
  }
  return res;
}

template <class IT, class VT>
VT ReduceDiagonalWeirdLayoutRotatingB_min(std::function<VT(VT, VT)> fn,
                                      VT init_accu_value,
                                      SpMat<IT, VT> *S,
                                      TransposeInfo &info) {
  int idx = 0, row_offset = 0, col_offset = 0;
  if (!GetDiagonalWeirdLayoutRotatingBOffsets<IT, VT>(
           info, idx, row_offset, col_offset))
    return init_accu_value;

  // Calculation.
  // TODO(penpornk): confirm with Sang that he wants natural log.
  VT res = init_accu_value;
  #pragma omp parallel for schedule(guided) reduction(min : res)
  for (int i = row_offset; i < S[idx].rows_; ++i) {
    for (int k = S[idx].headptrs_[i]; k < S[idx].headptrs_[i+1]; ++k) {
      int j = S[idx].indices_[k];
      if (i - row_offset == j - col_offset && i - row_offset >= 0) {
        res = fn(res, S[idx].values_[k]);
      }
    }
  }
  return res;
}

//
// Rewritten routines
//

//
// Computes dense (A) x dense (B)  = dense (C).
// 1D block row layout. Shift by c_A.
//
template <class IT, class VT>
void MMRow(DistDMat<IT, VT> &A,
           DistDMat<IT, VT> &B,
           DistDMat<IT, VT> &C, Timer &T) {
  T.Start("MMRow");
  // Must all be 1D block row layout.
  assert(A.layout() == ONED_BLOCK_ROW &&
         B.layout() == ONED_BLOCK_ROW &&
         C.layout() == ONED_BLOCK_ROW);

  // C must have the same number of layers as A.
  assert(C.layers() == A.layers());
  C.SetSize(A.rows(), B.cols());
  T.Start("MMRow: fill");
  C.mat_.Fill(0.0);
  T.Stop("MMRow: fill");

  // Finds offset.
  int rounds = B.teams() / A.layers();
  int layer_id = std::min(A.layer_id(), B.layer_id());
  int c_ratio = std::max(1, A.layers() / B.layers());
  int start_offset = layer_id * c_ratio;
  int lrows = A.lrows();

  T.Start("MMRow: shift");
  B.Shift(LAYER_COL, start_offset);
  T.Stop("MMRow: shift");
  for (int i = 0; i < rounds; ++i) {
    int row_id  = B.owner_rank_[LAYER_COL];
    DMat<IT, VT> subA;
    subA.ShallowSubDMat(A.mat_, 0, B.row_displs_[row_id], lrows, B.row_counts_[row_id]);
    T.Start("MMRow: matmul");
    subA.MultiplyAdd(B.mat_, C.mat_);
    T.Stop("MMRow: matmul");

    T.Start("MMRow: shift");
    B.Shift(LAYER_COL, A.layers());
    T.Stop("MMRow: shift");
  }
  T.Start("MMRow: allreduce");
  C.Allreduce(TEAM);
  T.Stop("MMRow: allreduce");
  T.Stop("MMRow");
}

//
// Computes sparse (A) x dense (B) = dense (C)
// 1D block row layout. Shift by c_A.
//
template <class IT, class VT>
void MMRow(DistSpMat<IT, VT> &A,
           DistDMat<IT, VT>  &B,
           DistDMat<IT, VT>  &C, Timer &T) {
  T.Start("MMRow");
  // Must all be 1D block row layout.
  assert(A.layout() == ONED_BLOCK_ROW &&
         B.layout() == ONED_BLOCK_ROW &&
         C.layout() == ONED_BLOCK_ROW);

  // C must have the same number of layers as A.
  assert(C.layers() == A.layers());
  C.SetSize(A.rows(), B.cols());
  T.Start("MMRow: fill");
  C.mat_.Fill(0.0);
  T.Stop("MMRow: fill");

  // Finds offset.
  int rounds = B.teams() / A.layers();
  int layer_id = std::min(A.layer_id(), B.layer_id());
  int c_ratio = std::max(1, A.layers() / B.layers());
  int start_offset = layer_id * c_ratio;
  int lrows = A.lrows();

  T.Start("MMRow: shift");
  B.Shift(LAYER_COL, start_offset);
  T.Stop("MMRow: shift");

  // Makes submatrices.
  IT row_counts [] = {A.mat_.rows_};
  IT row_displs [] = {0, A.mat_.rows_};
  SpMat<IT, VT> *submats = A.mat_.PartitionToSubmatrices(1, row_counts, row_displs,
                                                         B.comm_[LAYER_COL].size, B.row_counts_, B.row_displs_);

  for (int i = 0; i < rounds; ++i) {
    int row_id  = B.owner_rank_[LAYER_COL];
    T.Start("MMRow: matmul");
    submats[row_id].MultiplyAdd(B.mat_, C.mat_);
    assert(row_id < B.comm_[LAYER_COL].size);
    T.Stop("MMRow: matmul");

    T.Start("MMRow: shift");
    B.Shift(LAYER_COL, A.layers());
    T.Stop("MMRow: shift");
  }
  T.Start("MMRow: allreduce");
  C.Allreduce(TEAM);
  T.Stop("MMRow: allreduce");
  T.Stop("MMRow");
}

//
// Computes dense (A) x dense (B)  = dense (C).
// 1D block column layout. Shift by c_B.
//
template <class IT, class VT>
void MMCol(DistDMat<IT, VT> &A,
           DistDMat<IT, VT> &B,
           DistDMat<IT, VT> &C, Timer &T) {
  T.Start("MMCol");
  // Must all be 1D block column layout.
  assert(A.layout() == ONED_BLOCK_COLUMN &&
         B.layout() == ONED_BLOCK_COLUMN &&
         C.layout() == ONED_BLOCK_COLUMN);

  // C must have the same number of layers as B.
  assert(C.layers() == B.layers());
  C.SetSize(A.rows(), B.cols());
  T.Start("MMCol: fill");
  C.mat_.Fill(0.0);
  T.Stop("MMCol: fill");

  // Finds offset.
  int rounds = A.teams() / B.layers();
  int layer_id = std::min(A.layer_id(), B.layer_id());
  int c_ratio = std::max(1, B.layers() / A.layers());
  int start_offset = layer_id * c_ratio;
  int lcols = B.lcols();

  T.Start("MMCol: shift");
  A.Shift(LAYER_ROW, start_offset);
  T.Stop("MMCol: shift");
  for (int i = 0; i < rounds; ++i) {
    int col_id  = A.owner_rank_[LAYER_ROW];
    DMat<IT, VT> subB;
    subB.ShallowSubDMat(B.mat_, A.col_displs_[col_id], 0, A.col_counts_[col_id], lcols);
    T.Start("MMCol: matmul");
    A.mat_.MultiplyAdd(subB, C.mat_);
    T.Stop("MMCol: matmul");

    T.Start("MMCol: shift");
    A.Shift(LAYER_ROW, B.layers());
    T.Stop("MMCol: shift");
  }
  T.Start("MMCol: allreduce");
  C.Allreduce(TEAM);
  T.Stop("MMCol: allreduce");
  T.Stop("MMCol");
}

//
// Computes sparse (A) x dense (B)  = dense (C).
// 1D block column layout. Shift by c_B.
//
template <class IT, class VT>
void MMCol(DistSpMat<IT, VT> &A,
           DistDMat<IT, VT>  &B,
           DistDMat<IT, VT>  &C, Timer &T) {
  T.Start("MMCol");
  // Must all be 1D block column layout.
  assert(A.layout() == ONED_BLOCK_COLUMN &&
         B.layout() == ONED_BLOCK_COLUMN &&
         C.layout() == ONED_BLOCK_COLUMN);

  // C must have the same number of layers as B.
  assert(C.layers() == B.layers());
  C.SetSize(A.rows(), B.cols());
  T.Start("MMCol: fill");
  C.mat_.Fill(0.0);
  T.Stop("MMCol: fill");

  // Finds offset.
  int rounds = A.teams() / B.layers();
  int layer_id = std::min(A.layer_id(), B.layer_id());
  int c_ratio = std::max(1, B.layers() / A.layers());
  int start_offset = layer_id * c_ratio;
  int lcols = B.lcols();

  T.Start("MMCol: shift");
  A.Shift(LAYER_ROW, start_offset);
  T.Stop("MMCol: shift");
  for (int i = 0; i < rounds; ++i) {
    int col_id  = A.owner_rank_[LAYER_ROW];
    DMat<IT, VT> subB;
    subB.ShallowSubDMat(B.mat_, A.col_displs_[col_id], 0, A.col_counts_[col_id], lcols);
    T.Start("MMCol: matmul");
    A.mat_.MultiplyAdd(subB, C.mat_);
    T.Stop("MMCol: matmul");

    T.Start("MMCol: shift");
    A.Shift(LAYER_ROW, B.layers());
    T.Stop("MMCol: shift");
  }
  T.Start("MMCol: allreduce");
  C.Allreduce(TEAM);
  T.Stop("MMCol: allreduce");
  T.Stop("MMCol");
}

//
// Computes dense (A) x dense (B)  = dense (C).
// 1D block row layout x 1D block column layout. Shift by c_B.
//
//
template <class IT, class VT>
void MMInnerASumReduce(DistDMat<IT, VT> &A,
                       DistDMat<IT, VT> &B,
                       DistDMat<IT, VT> &C, Timer &T) {
  T.Start("MMInnerASD");
  // Must all be 1D block column layout.
  assert(A.layout() == ONED_BLOCK_ROW &&
         B.layout() == ONED_BLOCK_COLUMN &&
         C.layout() == ONED_BLOCK_COLUMN);

  // C must have the same number of layers as B.
  assert(C.layers() == B.layers());
  C.SetSize(A.rows(), B.cols());
  T.Start("MMInnerASD: fill");
  C.mat_.Fill(0.0);
  T.Stop("MMInnerASD: fill");

  // Finds offset.
  int rounds = A.teams() / B.layers();
  int layer_id = std::min(A.layer_id(), B.layer_id());
  int c_ratio = std::max(1, B.layers() / A.layers());
  int start_offset = layer_id * c_ratio;
  int lcols = B.lcols();

  T.Start("MMInnerASD: shift");
  A.Shift(LAYER_COL, start_offset);
  T.Stop("MMInnerASD: shift");
  for (int i = 0; i < rounds; ++i) {
    int row_id  = A.owner_rank_[LAYER_COL];
    DMat<IT, VT> subC;
    subC.ShallowSubDMat(C.mat_, A.row_displs_[row_id], 0, A.row_counts_[row_id], lcols);
    T.Start("MMInnerASD: matmul");
    A.mat_.MultiplyAdd(B.mat_, subC);
    T.Stop("MMInnerASD: matmul");

    T.Start("MMInnerASD: shift");
    A.Shift(LAYER_COL, B.layers());
    T.Stop("MMInnerASD: shift");
  }
  T.Start("MMInnerASD: allreduce");
  C.Allreduce(TEAM);
  T.Stop("MMInnerASD: allreduce");
  T.Stop("MMInnerASD");
}

//
// Computes dense (A) x dense (B)  = dense (C).
// 1D block row layout x 1D block column layout. Shift by c_B.
//
//
template <class IT, class VT>
void MMInnerA(DistDMat<IT, VT> &A,
              DistDMat<IT, VT> &B,
              DistDMat<IT, VT> &C, Timer &T) {
  T.Start("MMInnerA");
  // Must all be 1D block column layout.
  assert(A.layout() == ONED_BLOCK_ROW &&
         B.layout() == ONED_BLOCK_COLUMN &&
         C.layout() == ONED_BLOCK_COLUMN);

  // C must have the same number of layers as B.
  assert(C.layers() == B.layers());
  C.SetSize(A.rows(), B.cols());
  T.Start("MMInnerA: fill");
  C.mat_.Fill(0.0);
  T.Stop("MMInnerA: fill");

  // Finds offset.
  int rounds = A.teams() / B.layers();
  int layer_id = std::min(A.layer_id(), B.layer_id());
  int c_ratio = std::max(1, B.layers() / A.layers());
  int start_offset = layer_id * c_ratio;
  int lcols = B.lcols();

  T.Start("MMInnerA: shift");
  A.Shift(LAYER_COL, start_offset);
  T.Stop("MMInnerA: shift");
  IT C_row_offset = 0;
  for (int i = 0; i < rounds; ++i) {
    int row_id  = A.owner_rank_[LAYER_COL];
    if (C.layer_id() == 0)
      C_row_offset = A.row_displs_[row_id];
    DMat<IT, VT> subC;
    subC.ShallowSubDMat(C.mat_, C_row_offset, 0, A.row_counts_[row_id], lcols);
    T.Start("MMInnerA: matmul");
    A.mat_.MultiplyAdd(B.mat_, subC);
    T.Stop("MMInnerA: matmul");
    C_row_offset += A.row_counts_[row_id];

    T.Start("MMInnerA: shift");
    A.Shift(LAYER_COL, B.layers());
    T.Stop("MMInnerA: shift");
  }

  T.Start("MMInnerA: gatherv");
  // Computes receive counts and displaces.
  int c = C.layers();
  int *recv_counts = new int[c];
  int *recv_displs = new int[c + 1];
  fill_vec(recv_counts, c, 0);
  for (int l = 1; l < c; ++l) {
    int lid = l % A.layers();
    int row_offset = l / A.layers();
    int row_id = (A.comm_[LAYER_COL].rank + row_offset + lid * c_ratio) % A.teams();
    for (int i = 0; i < rounds; ++i) {
      recv_counts[l] += A.row_counts_[row_id];
      row_id = (row_id + c) % A.teams();
    }
    recv_counts[l] *= lcols;
  }
  gen_displs_from_counts(c, recv_counts, recv_displs);

  // Gathers.
  VT *recv_buf = NULL;
  if (C.layer_id() == 0) {
    recv_buf = new VT[recv_displs[c]];
    MPI_Gatherv(C.mat_.values_, 0, C.mpi_vt_, recv_buf, recv_counts, recv_displs, C.mpi_vt_, 0, C.comm_[TEAM].comm);
  } else {
    MPI_Gatherv(C.mat_.values_, C_row_offset * lcols, C.mpi_vt_, recv_buf, NULL, NULL, C.mpi_vt_, 0, C.comm_[TEAM].comm);
  }

  T.Stop("MMInnerA: gatherv");
  T.Start("MMInnerA: copy");
  if (C.layer_id() == 0) {
    for (int l = 1; l < c; ++l) {
      int lid = l % A.layers();
      int row_offset = l / A.layers();
      int row_id = (A.comm_[LAYER_COL].rank + row_offset + lid * c_ratio) % A.teams();
      int buf_offset = 0;
      for (int i = 0; i < rounds; ++i) {
        int num_elements = A.row_counts_[row_id] * lcols;
        memcpy(C.mat_.values_ + A.row_displs_[row_id] * lcols, recv_buf + recv_displs[l] + buf_offset, num_elements * sizeof(VT));
        buf_offset += num_elements;
        row_id = (row_id + c) % A.teams();
      }
    }
    delete [] recv_counts;
    delete [] recv_displs;
    delete [] recv_buf;
  }
  T.Stop("MMInnerA: copy");

  T.Start("MMInnerA: Broadcast");
  C.Bcast(TEAM, 0);
  T.Stop("MMInnerA: Broadcast");

  T.Stop("MMInnerA");
}

//
// Computes sparse (A) x dense (B)  = dense (C).
// 1D block row layout x 1D block column layout. Shift by c_B.
//
//
template <class IT, class VT>
void MMInnerA(DistSpMat<IT, VT> &A,
              DistDMat<IT, VT>  &B,
              DistDMat<IT, VT>  &C, Timer &T) {
  T.Start("MMInnerA");
  // Must all be 1D block column layout.
  assert(A.layout() == ONED_BLOCK_ROW &&
         B.layout() == ONED_BLOCK_COLUMN &&
         C.layout() == ONED_BLOCK_COLUMN);

  // C must have the same number of layers as B.
  assert(C.layers() == B.layers());
  C.SetSize(A.rows(), B.cols());
  T.Start("MMInnerA: fill");
  C.mat_.Fill(0.0);
  T.Stop("MMInnerA: fill");

  // Finds offset.
  int rounds = A.teams() / B.layers();
  int layer_id = std::min(A.layer_id(), B.layer_id());
  int c_ratio = std::max(1, B.layers() / A.layers());
  int start_offset = layer_id * c_ratio;
  int lcols = B.lcols();

  T.Start("MMInnerA: shift");
  A.Shift(LAYER_COL, start_offset);
  T.Stop("MMInnerA: shift");
  IT C_row_offset = 0;
  for (int i = 0; i < rounds; ++i) {
    int row_id  = A.owner_rank_[LAYER_COL];
    if (C.layer_id() == 0)
      C_row_offset = A.row_displs_[row_id];
    DMat<IT, VT> subC;
    subC.ShallowSubDMat(C.mat_, C_row_offset, 0, A.row_counts_[row_id], lcols);
    T.Start("MMInnerA: matmul");
    A.mat_.MultiplyAdd(B.mat_, subC);
    T.Stop("MMInnerA: matmul");
    C_row_offset += A.row_counts_[row_id];

    T.Start("MMInnerA: shift");
    A.Shift(LAYER_COL, B.layers());
    T.Stop("MMInnerA: shift");
  }

  T.Start("MMInnerA: gatherv");
  // Computes receive counts and displaces.
  int c = C.layers();
  int *recv_counts = new int[c];
  int *recv_displs = new int[c + 1];
  fill_vec(recv_counts, c, 0);
  for (int l = 1; l < c; ++l) {
    int lid = l % A.layers();
    int row_offset = l / A.layers();
    int row_id = (A.comm_[LAYER_COL].rank + row_offset + lid * c_ratio) % A.teams();
    for (int i = 0; i < rounds; ++i) {
      recv_counts[l] += A.row_counts_[row_id];
      row_id = (row_id + c) % A.teams();
    }
    recv_counts[l] *= lcols;
  }
  gen_displs_from_counts(c, recv_counts, recv_displs);

  // Gathers.
  VT *recv_buf = NULL;
  if (C.layer_id() == 0) {
    recv_buf = new VT[recv_displs[c]];
    MPI_Gatherv(C.mat_.values_, 0, C.mpi_vt_, recv_buf, recv_counts, recv_displs, C.mpi_vt_, 0, C.comm_[TEAM].comm);
  } else {
    MPI_Gatherv(C.mat_.values_, C_row_offset * lcols, C.mpi_vt_, recv_buf, NULL, NULL, C.mpi_vt_, 0, C.comm_[TEAM].comm);
  }

  T.Stop("MMInnerA: gatherv");
  T.Start("MMInnerA: copy");
  if (C.layer_id() == 0) {
    for (int l = 1; l < c; ++l) {
      int lid = l % A.layers();
      int row_offset = l / A.layers();
      int row_id = (A.comm_[LAYER_COL].rank + row_offset + lid * c_ratio) % A.teams();
      int buf_offset = 0;
      for (int i = 0; i < rounds; ++i) {
        int num_elements = A.row_counts_[row_id] * lcols;
        memcpy(C.mat_.values_ + A.row_displs_[row_id] * lcols, recv_buf + recv_displs[l] + buf_offset, num_elements * sizeof(VT));
        buf_offset += num_elements;
        row_id = (row_id + c) % A.teams();
      }
    }
    delete [] recv_counts;
    delete [] recv_displs;
    delete [] recv_buf;
  }
  T.Stop("MMInnerA: copy");

  T.Start("MMInnerA: Broadcast");
  C.Bcast(TEAM, 0);
  T.Stop("MMInnerA: Broadcast");

  T.Stop("MMInnerA");
}

}  // namespace spdm3
