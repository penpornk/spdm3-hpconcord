#include "distmatmul.h"

#include <cstdio>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <sstream>
#include <tuple>
#include <utility>

#include "comm.h"
#include "distdmat.h"
#include "distspmat.h"
#include "distio.h"
#include "timer.h"
#include "utility.h"

#define IT  int
#define VT  double

namespace spdm3 {

Timer T;

bool TestInnerAbc(int dummy, int dummy2) {
  Comm *c1Drow = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW);
  Comm *c1Dcol = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN);
  
  DistSpMat<IT, VT> A(c1Drow);
  A.GenerateUniform(159, 281, 0.5);
  DistDMat<IT, VT> B(c1Dcol), C(c1Dcol);
  B.GenerateWithFunction(281, 170);
  innerABC_shiftc(A, B, C);
  
  SpMat<IT, VT> wa = A.GetWholeMatrix();
  DMat<IT, VT> wb = B.GetWholeMatrix();
  DMat<IT, VT> wc = C.GetWholeMatrix();
  DMat<IT, VT> c;
  wa.MultiplyAdd(wb, c);
  
  delete [] c1Drow;
  delete [] c1Dcol;
  return c.IsEquivalent(wc);
}

bool TestInnerAbcC(int layers, int dummy) {
  Comm *c1Drow = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW);
  Comm *c1Dcol = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN);
  
  Comm *c1Drow_new = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers);
  Comm *c1Dcol_new = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, layers);
  
  // Declarations.
  DistSpMat<IT, VT> A(c1Drow);
  DistDMat<IT, VT> B(c1Dcol), C(c1Dcol_new);
  DMat<IT, VT> c, subc, subC;
  
  // Generations.
  A.GenerateUniform(159, 281, 0.5);
  B.GenerateWithFunction(281, 170);
  A.Replicate(c1Drow_new);
  B.Replicate(c1Dcol_new);
  
  // Gets whole matrix to compute correct answer locally.
  SpMat<IT, VT> wa = A.GetWholeMatrix();
  DMat<IT, VT> wb = B.GetWholeMatrix();
  wa.MultiplyAdd(wb, c);
  
  // Calculations.
  innerABC_shiftc(A, B, C);
  
  // Checks the answer.
  bool res = true;
  int rounds = A.teams() / layers;
  int row_id = (A.layer_id() + A.team_id()) % layers;
  int row_offset = 0;
  for (int r = 0; r < rounds; ++r) {
    subc.SubDMat(c, A.row_displs_[row_id], B.col_displs_[A.team_id()],
                    A.row_counts_[row_id], B.col_counts_[A.team_id()]);
    subC.SubDMat(C.mat_, row_offset, 0,
                         A.row_counts_[row_id], C.lcols());
    res &= subc.IsEquivalent(subC);
    row_offset += A.row_counts_[row_id];
    row_id = row_id + layers;
  }
  
  delete [] c1Drow;
  delete [] c1Dcol;
  delete [] c1Drow_new;
  delete [] c1Dcol_new;
  
  return res;
}

bool TestInnerAbcWTrace(int dummy, int dummy2) {
  Comm *c1Drow = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW);
  Comm *c1Dcol = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN);
  
  DistSpMat<IT, VT> A(c1Drow);
  A.GenerateSymmetric(181, 181, 0.5, c1Drow[WORLD].size * 2);
  DistDMat<IT, VT> B(c1Dcol), C(c1Dcol);
  B.GenerateWithFunction(181, 181);
  
  Timer T;
  T.Init(MPI_COMM_WORLD);
  VT trace = innerABC_shiftc_w_trace(A, B, C, T);
  MPI_Allreduce(MPI_IN_PLACE, &trace, 1, A.mpi_vt_, MPI_SUM, A.comm_[WORLD].comm);
  
  SpMat<IT, VT> wa = A.GetWholeMatrix();
  DMat<IT, VT> wb = B.GetWholeMatrix();
  DMat<IT, VT> wc = C.GetWholeMatrix();
  DMat<IT, VT> c;
  
  SpMat<IT, VT> omega;
  omega.DeepCopy(wa);
  wa.MultiplyAdd(wb, c);
  omega.DotMultiplyI(c);
  VT actual_trace = omega.Reduce(SPDM3_SUM);
  
  bool res = c.IsEquivalent(wc);
  res &= FEQ(trace, actual_trace);
  
  delete [] c1Drow;
  delete [] c1Dcol;
  return res;
}

bool TestInnerAbcWTraceC(int layers, int dummy) {
  Comm *c1Drow = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW);
  Comm *c1Dcol = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN);
  
  Comm *c1Drow_new = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers);
  Comm *c1Dcol_new = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, layers);
  
  // Declarations.
  DistSpMat<IT, VT> A(c1Drow);
  DistDMat<IT, VT> B(c1Dcol), C(c1Dcol_new);
  DMat<IT, VT> c, subc, subC;
  
  // Generations.
  A.GenerateUniform(181, 181, 0.5);
  B.GenerateWithFunction(181, 181);
  A.Replicate(c1Drow_new);
  B.Replicate(c1Dcol_new);
  
  // Gets whole matrix to compute correct answer locally.
  SpMat<IT, VT> wa = A.GetWholeMatrix();
  DMat<IT, VT> wb = B.GetWholeMatrix();
  wa.MultiplyAdd(wb, c);
  
  SpMat<IT, VT> omega;
  omega.DeepCopy(wa);
  omega.DotMultiplyI(c);
  VT actual_trace = omega.Reduce(SPDM3_SUM);
  
  // Calculations.
  VT trace = innerABC_shiftc_w_trace(A, B, C, T);
  MPI_Allreduce(MPI_IN_PLACE, &trace, 1, A.mpi_vt_, MPI_SUM, A.comm_[WORLD].comm);
  
  // Checks the answer.
  bool res = true;
  int rounds = A.teams() / layers;
  int row_id = (A.layer_id() + A.team_id()) % layers;
  int row_offset = 0;
  for (int r = 0; r < rounds; ++r) {
    subc.SubDMat(c, A.row_displs_[row_id], B.col_displs_[A.team_id()],
                    A.row_counts_[row_id], B.col_counts_[A.team_id()]);
    subC.SubDMat(C.mat_, row_offset, 0,
                         A.row_counts_[row_id], C.lcols());
    res &= subc.IsEquivalent(subC);
    row_offset += A.row_counts_[row_id];
    row_id = row_id + layers;
  }
  res &= FEQ(trace, actual_trace);
  
  delete [] c1Drow;
  delete [] c1Dcol;
  delete [] c1Drow_new;
  delete [] c1Dcol_new;
  
  return res;
}

bool TestInnerAbcTranspose(int dummy, int dummy2) {
  Comm *c1Dcol = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN);
  DistDMat<IT, VT> C(c1Dcol), T(c1Dcol);
  C.GenerateWithFunction(211, 211);
  
  Timer t;
  t.Init(MPI_COMM_WORLD);
  innerABC_shiftc_transpose(C, T, t);
  
  DMat<IT, VT> WC = C.GetWholeMatrix();
  DMat<IT, VT> WT = T.GetWholeMatrix();
  DMat<IT, VT> ans;
  ans.Transpose(WC);

  delete [] c1Dcol;
  return ans.IsEquivalent(WT);
}

bool TestInnerAbcTransposeC(int layers, int dummy) {
  Comm *c1Drow = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW);
  Comm *c1Dcol = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN);
  
  Comm *c1Drow_new = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers);
  Comm *c1Dcol_new = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, layers);
  
  // Declarations.
  DistSpMat<IT, VT> A(c1Drow);
  DistDMat<IT, VT> B(c1Dcol), C(c1Dcol_new);
  
  // Generations.
  A.GenerateUniform(159, 159, 0.5);
  B.GenerateWithFunction(159, 159);
  A.Replicate(c1Drow_new);
  B.Replicate(c1Dcol_new);

  // Gets whole matrix to compute correct answer locally.
  // Must be called before innerABC_shiftc or A is ruined.
  SpMat<IT, VT> wa = A.GetWholeMatrix();
  DMat<IT, VT> wb = B.GetWholeMatrix(), wc, ans;
  wa.MultiplyAdd(wb, wc);
  ans.Transpose(wc);
  
  // Creates C.
  innerABC_shiftc(A, B, C);

  // Creates T.
  Timer t;
  t.Init(MPI_COMM_WORLD);
  DistDMat<IT, VT> T(c1Dcol_new);
  innerABC_shiftc_transpose(C, T, t);

  // Checks the answer.
  bool res = true;
  int rounds = A.teams() / layers;
  int row_id = (A.layer_id() + A.team_id()) % layers;
  int row_offset = 0;
  DMat<IT, VT> suba, subt;
  for (int r = 0; r < rounds; ++r) {
    suba.SubDMat(ans, A.row_displs_[row_id], B.col_displs_[A.team_id()],
                      A.row_counts_[row_id], B.col_counts_[A.team_id()]);
    subt.SubDMat(T.mat_, row_offset, 0,
                         A.row_counts_[row_id], T.lcols());
    res &= suba.IsEquivalent(subt);
    row_offset += A.row_counts_[row_id];
    row_id = row_id + layers;
  }

  delete [] c1Drow;
  delete [] c1Dcol;
  delete [] c1Drow_new;
  delete [] c1Dcol_new;
  
  return res;
}

bool TestInnerAbcReformSparseBlockColumn(int layers, int dummy) {
  Comm *c1Drow_new = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers);
  Comm *c1Dcol_new = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, layers);
  
  // Declarations.
  DistDMat<IT, VT> C(c1Dcol_new);
  DistDMat<IT, VT> W(c1Dcol_new);
  DistSpMat<IT, VT> S(c1Drow_new);
  
  // Generates C and prunes with probability 0.5.
  C.GenerateWithFunction(159, 159);
  for (int i = 0; i < C.lrows(); ++i) {
    for (int j = 0; j < C.lcols(); ++j) {
      if (rand() % 100 < 50) {
        C.mat_.values_[i * C.mat_.lda_ + j] = 0;
      }
    }
  }
  DMat<IT, VT> wc = C.GetWholeMatrix(), ct;
  ct.Transpose(wc);
  
  // Performs.
  W.WeirdLayoutBlockColumn(C);
  innerABC_shiftc_reform_sparse_from_blockColumn(W, S);
  
  // Calculates answer.
  SpMat<IT, VT> ws = S.GetWholeMatrix();
  DMat<IT, VT> ds(ws);
  
  bool res = true;
  res &= ds.IsEquivalent(ct);

  delete [] c1Drow_new;
  delete [] c1Dcol_new;
  
  return res;
}

bool TestInnerAbcReformSparseBlockRow(int layers, int dummy) {
  Comm *c1Drow_new = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers);
  Comm *c1Dcol_new = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, layers);
  
  // Declarations.
  DistDMat<IT, VT> C(c1Dcol_new);
  DistDMat<IT, VT> W(c1Dcol_new), WT(c1Drow_new);
  DistSpMat<IT, VT> S(c1Drow_new);
  
  // Generates C and prunes with probability 0.5.
  C.GenerateWithFunction(157, 157);
  for (int i = 0; i < C.lrows(); ++i) {
    for (int j = 0; j < C.lcols(); ++j) {
      if (rand() % 100 < 50) {
        C.mat_.values_[i * C.mat_.lda_ + j] = 0;
      }
    }
  }
  DMat<IT, VT> wc = C.GetWholeMatrix(), ct;
  ct.Transpose(wc);
  
  // Performs.
  W.WeirdLayoutBlockColumn(C);
  WT.Transpose(W);
  innerABC_shiftc_reform_sparse_from_blockRow(WT, S, T);

  // Calculates answer.
  SpMat<IT, VT> ws = S.GetWholeMatrix();
  DMat<IT, VT> ds(ws);
  
  bool res = true;
  res &= ds.IsEquivalent(ct);

  delete [] c1Drow_new;
  delete [] c1Dcol_new;
  
  return res;
}

// DDM^3: Dense-Dense Matrix-Matrix Multiplication.
bool TestInnerAbcShift1DDM3(int dummy, int dummy2) {
  Comm *c1Drow = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW);
  Comm *c1Dcol = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN);
  
  DistDMat<IT, VT> A(c1Drow);
  A.GenerateWithFunction(159, 281);
  DistDMat<IT, VT> B(c1Dcol), C(c1Dcol);
  B.GenerateWithFunction(281, 170);
  innerABC_shift1(A, B, C);
  
  DMat<IT, VT> wa = A.GetWholeMatrix();
  DMat<IT, VT> wb = B.GetWholeMatrix();
  DMat<IT, VT> wc = C.GetWholeMatrix();
  DMat<IT, VT> c;
  wa.MultiplyAdd(wb, c);
  
  delete [] c1Drow;
  delete [] c1Dcol;
  return c.IsEquivalent(wc);
}

bool TestAllBlkRowShift1(int c_s, int c_d) {
  Comm *cs = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, c_s);
  Comm *cd = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, c_d);
  
  DistSpMat<IT, VT> A(cs);
  A.GenerateUniform(123, 151, 0.5);
  DistDMat<IT, VT> B(cd), C(cs);
  B.GenerateWithFunction(151, 137);
  
  SpMat<IT, VT> wa = A.GetWholeMatrix();
  DMat<IT, VT> wb = B.GetWholeMatrix();
  
  allblkrow_shift1(A, B, C);

  DMat<IT, VT> wc = C.GetWholeMatrix();
  DMat<IT, VT> c;
  wa.MultiplyAdd(wb, c);

  delete [] cs;
  delete [] cd;
  return c.IsEquivalent(wc);
}

bool TestAllBlkRowShiftc(int c_s, int c_d) {
  Comm *cs = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, c_s);
  Comm *cd = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, c_d);
  
  DistSpMat<IT, VT> A(cs);
  A.GenerateUniform(123, 151, 0.5);
  DistDMat<IT, VT> B(cd), C(cs);
  B.GenerateWithFunction(151, 137);
  
  SpMat<IT, VT> wa = A.GetWholeMatrix();
  DMat<IT, VT> wb = B.GetWholeMatrix();
  
  allblkrow_shiftc(A, B, C);

  DMat<IT, VT> wc = C.GetWholeMatrix();
  DMat<IT, VT> c;
  wa.MultiplyAdd(wb, c);

  delete [] cs;
  delete [] cd;
  return c.IsEquivalent(wc);
}

bool TestInnerShiftcRotatingB(int c_f, int c_r) {
  Comm *cf = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, c_f);
  Comm *cr = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, c_r);
  
  int n = 171;
  DistDMat<IT, VT> A(cf), W(cf);
  A.GenerateWithFunction(n, n);
  DistDMat<IT, VT> B(cr), C(cf);
  
  // Creating Identity matrix.
  B.SetSize(n, n);
  B.mat_.Fill(0.0);
  for (int j = 0; j < B.lcols(); ++j) {
    int i = B.col_displs_[B.team_id()] + j;
    B.mat_.values_[i * B.llda() + j] = 1.0;
  }
  W.WeirdLayoutBlockRowRotatingB(A, B);

  DMat<IT, VT> wa = A.GetWholeMatrix();
  DMat<IT, VT> wb = B.GetWholeMatrix();
  // TODO(penpornk): See why weirdlayout has problem with GetWholeMatrix.
//  DMat<IT, VT> ww = W.GetWholeMatrix();

  inner_shiftc_rotatingB(A, B, C, T);

  delete [] cf;
  delete [] cr;
  return W.mat_.IsEquivalent(C.mat_);
}

bool TestInnerShitfcRotatingBTranspose(int layers1, int layers2) {
  Comm *c1Drow = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers1);
  Comm *c1Dcol = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, layers2);
  DistDMat<IT, VT> C(c1Drow), CT(c1Drow), B(c1Dcol);
  
  int n = 179;
  C.SetSize(n, n);
  B.GenerateWithFunction(n, n);
  
  // Fill series.
  C.mat_.Allocate();
  fill_series(C.mat_.values_, C.lrows() * C.lcols(), (VT) (C.row_displs_[C.team_id()] * C.global_cols_));
  
  DistDMat<IT, VT> W(c1Drow);
  W.WeirdLayoutBlockRowRotatingB(C, B);
  
  TransposeInfo info;
  createTransposeInfo_inner_shiftc_rotatingB(C, B, info, T);
  inner_shiftc_rotatingB_transpose(W, CT, info, T);

  // Checks answers.
  DistDMat<IT, VT> MockCT(c1Drow), MockWT(c1Drow);
  MockCT.SetSize(C.rows(), C.cols());
  MockCT.mat_.Allocate();
  for (int i = 0; i < MockCT.lrows(); ++i) {
    int global_i = MockCT.row_displs_[MockCT.team_id()] + i;
    for (int j = 0; j < MockCT.lcols(); ++j) {
      int global_j = j;
      MockCT.mat_.values_[i * MockCT.mat_.lda_ + j] =
        global_j * C.cols() + global_i;
    }
  }
  MockWT.WeirdLayoutBlockRowRotatingB(MockCT, B);
  
  delete [] c1Drow;
  delete [] c1Dcol;
  return MockWT.mat_.IsEquivalent(CT.mat_);
}

// DDM^3: Dense-Dense Matrix-Matrix Multiplication.
bool TestInnerAbcBcastDDM3(int layers, int dummy) {
  Comm *c1Drow = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers);
  Comm *c1Dcol = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, layers);
  
  DistDMat<IT, VT> A(c1Drow);
  A.GenerateWithFunction(159, 281);
  DistDMat<IT, VT> B(c1Dcol), C(c1Dcol);
  B.GenerateWithFunction(281, 170);
  innerABC_bcast(A, B, C, T);
  
  DMat<IT, VT> wa = A.GetWholeMatrix();
  DMat<IT, VT> wb = B.GetWholeMatrix();
  DMat<IT, VT> wc = C.GetWholeMatrix();
  DMat<IT, VT> c;
  wa.MultiplyAdd(wb, c);
  
  delete [] c1Drow;
  delete [] c1Dcol;
  
  return c.IsEquivalent(wc);
}

bool TestReduceDiagonalWeirdLayoutRotatingB(int layers1, int layers2) {
  Comm *c1Drow = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers1);
  Comm *c1Dcol = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, layers2);
  
  int n = 193;
  DistSpMat<IT, VT> A(c1Drow);
  A.GenerateUniform(n, n, 0.5);
  DistDMat<IT, VT> Adummy(c1Drow);
  Adummy.GenerateWithFunction(n, n);
  DistDMat<IT, VT> B(c1Dcol);
  B.GenerateWithFunction(n, n);
  
  TransposeInfo info;
  createTransposeInfo_inner_shiftc_rotatingB(Adummy, B, info, T);

  SpMat<IT, VT> *S = GetWeirdLayoutRotatingBSpMats(A, B);
  
  std::function<VT(VT, VT)> fn = [](VT accu, VT operand)->VT {
                                    return accu + log(operand);
                                  };
  VT partial_logdet = ReduceDiagonalWeirdLayoutRotatingB(fn, 0.0, S, info);

  DistSpMat<IT, VT> D(c1Drow);
  D.Diagonal(A);
  D.ElmtWiseOp(log);
  VT actual_partial_logdet = D.mat_.Reduce(SPDM3_SUM);

  VT logdet, actual_logdet;
  MPI_Allreduce(&partial_logdet, &logdet, 1, D.mpi_vt_, MPI_SUM, D.comm_[WORLD].comm);
  MPI_Allreduce(&actual_partial_logdet, &actual_logdet, 1, D.mpi_vt_, MPI_SUM, D.comm_[LAYER].comm);
  
  delete [] c1Drow;
  delete [] c1Dcol;
  delete [] S;
  
  return FEQ(logdet, actual_logdet);
}

bool TestOpDiagonalWeirdLayoutRotatingB(int layers1, int layers2) {
  Comm *c1Drow = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, layers1);
  Comm *c1Dcol = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, layers2);
  
  int n = 193;
  DistSpMat<IT, VT> A(c1Drow);
  A.GenerateUniform(n, n, 0.5);
  DistDMat<IT, VT> Adummy(c1Drow);
  Adummy.GenerateWithFunction(n, n);
  DistDMat<IT, VT> B(c1Dcol);
  B.GenerateWithFunction(n, n);
  
  TransposeInfo info;
  createTransposeInfo_inner_shiftc_rotatingB(Adummy, B, info, T);

  DMat<IT, VT> *G = GetWeirdLayoutRotatingBDMats(Adummy, B);
  SpMat<IT, VT> *S = GetWeirdLayoutRotatingBSpMats(A, B);
  
  std::function<VT(VT, VT)> add = [](VT a, VT b)->VT { return a + b; };
  OpDiagonalWeirdLayoutRotatingB(add, G, S, info);

  DistSpMat<IT, VT> D(c1Drow);
  D.Diagonal(A);
  Adummy.ElmtWiseOp(add, D);
  DMat<IT, VT> *E = GetWeirdLayoutRotatingBDMats(Adummy, B);
  
  bool res = true;
  for (int i = 0; i < info.num_grids_in_1D; ++i) {
    res &= E[i].IsEquivalent(G[i]);
  }
  
  delete [] c1Drow;
  delete [] c1Dcol;
  delete [] S;
  delete [] G;
  delete [] E;
  
  return res;
}

bool TestInnerShiftcRotatingBAsync(int c_f, int c_r) {
  Comm *cf = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, c_f);
  Comm *cr = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, c_r);
  
  int n = 160; //171;
  DistDMat<IT, VT> A(cf), W(cf);
  A.GenerateWithFunction(n, n);
  DistDMat<IT, VT> B(cr), C(cf);
  
  // Creating Identity matrix.
  B.SetSize(n, n);
  B.mat_.Fill(0.0);
  B.ShiftWait();
  B.ShiftAsync(LAYER_ROW, 1);
  B.ShiftWait();
  B.ShiftAsync(LAYER_ROW, 1);
  B.print("A.layers() = %d\n", A.layers());
//  for (int j = 0; j < B.lcols(); ++j) {
//    int i = B.col_displs_[B.team_id()] + j;
//    B.mat_.values_[i * B.llda() + j] = 1.0;
//  }
//  W.WeirdLayoutBlockRowRotatingB(A, B);
//
//  DMat<IT, VT> wa = A.GetWholeMatrix();
//  DMat<IT, VT> wb = B.GetWholeMatrix();
  // TODO(penpornk): See why weirdlayout has problem with GetWholeMatrix.
//  DMat<IT, VT> ww = W.GetWholeMatrix();

//  inner_shiftc_rotatingB_async(A, B, C, T);

  return true;
//  delete [] cf;
//  delete [] cr;
//  return W.mat_.IsEquivalent(C.mat_);
}

bool TestDummy(int c_f, int c_r) {
  int n = 160;
  Comm *cr = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, c_r);
  
//  DMat<IT, VT> A(n/4, n);
  VT *array = new VT[n * n];
  VT *something = new VT[n * n];
  
  DistDMat<IT, VT> B(cr);
  B.SetSize(n, n);
  B.mat_.Fill(1);

  B.ShiftAsync(LAYER_ROW, 1);
  B.ShiftWait();
  B.ShiftAsync(LAYER_ROW, 1);
  
  delete [] array;
  delete [] something;
  delete [] cr;
  return true;
}

bool TestMMRowDenseDense(int c_a, int c_b) {
  int m = 1000;
  int n = 800;
  int k = 600;
  
  Timer T;
  T.Init(MPI_COMM_WORLD);
  
  Comm *commA = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, c_a);
  Comm *commB = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, c_b);
  
  DistDMat<IT, VT> A(commA);
  DistDMat<IT, VT> B(commB);
  DistDMat<IT, VT> C(commA);
  
  A.GenerateWithFunction(m, k);
  B.GenerateWithFunction(k, n);
  
  MMRow(A, B, C, T);
  
  DMat<IT, VT> Wa = A.GetWholeMatrix();
  DMat<IT, VT> Wb = B.GetWholeMatrix();
  DMat<IT, VT> Wc = C.GetWholeMatrix();
  
  DMat<IT, VT> res;
  Wa.MultiplyAdd(Wb, res);
  
  delete [] commA;
  delete [] commB;
  return res.IsEquivalent(Wc);
}

bool TestMMRowSparseDense(int c_a, int c_b) {
  int m = 500;
  int n = 300;
  int k = 400;
  double nnz_ratio = 0.3;
  
  Timer T;
  T.Init(MPI_COMM_WORLD);
  
  Comm *commA = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, c_a);
  Comm *commB = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, c_b);
  
  DistSpMat<IT, VT> A(commA);
  DistDMat<IT, VT>  B(commB);
  DistDMat<IT, VT>  C(commA);
  
  A.GenerateUniform(m, k, nnz_ratio);
  B.GenerateWithFunction(k, n);
  
  MMRow(A, B, C, T);
  
  SpMat<IT, VT> Wa = A.GetWholeMatrix();
  DMat<IT, VT>  Wb = B.GetWholeMatrix();
  DMat<IT, VT>  Wc = C.GetWholeMatrix();
  
  DMat<IT, VT> res;
  Wa.MultiplyAdd(Wb, res);
  
  delete [] commA;
  delete [] commB;
  return res.IsEquivalent(Wc);
}

bool TestMMColDenseDense(int c_a, int c_b) {
  int m = 500;
  int n = 700;
  int k = 400;
  
  Timer T;
  T.Init(MPI_COMM_WORLD);
  
  Comm *commA = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, c_a);
  Comm *commB = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, c_b);
  
  DistDMat<IT, VT> A(commA);
  DistDMat<IT, VT> B(commB);
  DistDMat<IT, VT> C(commB);
  
  A.GenerateWithFunction(m, k);
  B.GenerateWithFunction(k, n);
  
  MMCol(A, B, C, T);
  
  DMat<IT, VT> Wa = A.GetWholeMatrix();
  DMat<IT, VT> Wb = B.GetWholeMatrix();
  DMat<IT, VT> Wc = C.GetWholeMatrix();
  
  DMat<IT, VT> res;
  Wa.MultiplyAdd(Wb, res);
  
  delete [] commA;
  delete [] commB;
  return res.IsEquivalent(Wc);
}

bool TestMMColSparseDense(int c_a, int c_b) {
  int m = 100;
  int n = 100;
  int k = 100;
  double nnz_ratio = 0.3;
  
  Timer T;
  T.Init(MPI_COMM_WORLD);
  
  Comm *commA = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, c_a);
  Comm *commB = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, c_b);
  
  DistSpMat<IT, VT> A(commA);
  DistDMat<IT, VT>  B(commB);
  DistDMat<IT, VT>  C(commB);
  
  A.GenerateUniform(m, k, nnz_ratio);
  B.GenerateWithFunction(k, n);
  
  MMCol(A, B, C, T);
  
  SpMat<IT, VT> Wa = A.GetWholeMatrix();
  DMat<IT, VT>  Wb = B.GetWholeMatrix();
  DMat<IT, VT>  Wc = C.GetWholeMatrix();
  
  DMat<IT, VT> res;
  Wa.MultiplyAdd(Wb, res);
  
  delete [] commA;
  delete [] commB;
  return res.IsEquivalent(Wc);
}

bool TestMMInnerASumReduceDenseDense(int c_a, int c_b) {
  int m = 300;
  int n = 400;
  int k = 500;
  
  Timer T;
  T.Init(MPI_COMM_WORLD);
  
  Comm *commA = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, c_a);
  Comm *commB = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, c_b);
  
  DistDMat<IT, VT> A(commA);
  DistDMat<IT, VT> B(commB);
  DistDMat<IT, VT> C(commB);
  
  A.GenerateWithFunction(m, k);
  B.GenerateWithFunction(k, n);
  
  MMInnerASumReduce(A, B, C, T);
  
  DMat<IT, VT> Wa = A.GetWholeMatrix();
  DMat<IT, VT> Wb = B.GetWholeMatrix();
  DMat<IT, VT> Wc = C.GetWholeMatrix();
  
  DMat<IT, VT> res;
  Wa.MultiplyAdd(Wb, res);
  
  delete [] commA;
  delete [] commB;
  return res.IsEquivalent(Wc);
}

bool TestMMInnerADenseDense(int c_a, int c_b) {
  int m = 375;
  int n = 412;
  int k = 258;
  
  Timer T;
  T.Init(MPI_COMM_WORLD);
  
  Comm *commA = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, c_a);
  Comm *commB = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, c_b);
  
  DistDMat<IT, VT> A(commA);
  DistDMat<IT, VT> B(commB);
  DistDMat<IT, VT> C(commB);
  
  A.GenerateWithFunction(m, k);
  B.GenerateWithFunction(k, n);
  
  MMInnerA(A, B, C, T);
  
  DMat<IT, VT> Wa = A.GetWholeMatrix();
  DMat<IT, VT> Wb = B.GetWholeMatrix();
  DMat<IT, VT> Wc = C.GetWholeMatrix();
  
  DMat<IT, VT> res;
  Wa.MultiplyAdd(Wb, res);
  
  delete [] commA;
  delete [] commB;
  return res.IsEquivalent(Wc);
}

bool TestMMInnerASparseDense(int c_a, int c_b) {
  int m = 333;
  int n = 555;
  int k = 486;
  double nnz_ratio = 0.3;
  
  Timer T;
  T.Init(MPI_COMM_WORLD);
  
  Comm *commA = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, c_a);
  Comm *commB = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, c_b);
  
  DistSpMat<IT, VT> A(commA);
  DistDMat<IT, VT>  B(commB);
  DistDMat<IT, VT>  C(commB);
  
  A.GenerateUniform(m, k, nnz_ratio);
  B.GenerateWithFunction(k, n);
  
  MMInnerA(A, B, C, T);
  
  SpMat<IT, VT> Wa = A.GetWholeMatrix();
  DMat<IT, VT>  Wb = B.GetWholeMatrix();
  DMat<IT, VT>  Wc = C.GetWholeMatrix();
  
  DMat<IT, VT> res;
  Wa.MultiplyAdd(Wb, res);
  
  delete [] commA;
  delete [] commB;
  return res.IsEquivalent(Wc);
}


}  // namespace spdm3

#define ADDTEST(fn, name, layers_1, layers_2) \
    tests.push_back(std::make_tuple((std::function<bool(int, int)>) fn, name, layers_1, layers_2));

int main(int argc, char* argv[]) {
  // Registers test cases.
  std::list< std::tuple<std::function<bool(int, int)>, const char*, int, int> > tests;
  
  ADDTEST(spdm3::TestInnerAbc, "TestInnerAbc", 1, 0);
  ADDTEST(spdm3::TestInnerAbcC, "TestInnerAbc", 2, 0);
  ADDTEST(spdm3::TestInnerAbcC, "TestInnerAbc", 4, 0);
  ADDTEST(spdm3::TestInnerAbcWTrace, "TestInnerAbcWTrace", 1, 0);
  ADDTEST(spdm3::TestInnerAbcWTraceC, "TestInnerAbcWTrace", 2, 0);
  ADDTEST(spdm3::TestInnerAbcWTraceC, "TestInnerAbcWTrace", 4, 0);
  ADDTEST(spdm3::TestInnerAbcTranspose, "TestInnerAbcTranspose", 1, 0);
  ADDTEST(spdm3::TestInnerAbcTransposeC, "TestInnerAbcTranspose", 2, 0);
  ADDTEST(spdm3::TestInnerAbcTransposeC, "TestInnerAbcTranspose", 4, 0);
  
  ADDTEST(spdm3::TestInnerAbcReformSparseBlockColumn, "TestInnerAbcReformSparseBlockColumn", 1, 0);
  ADDTEST(spdm3::TestInnerAbcReformSparseBlockColumn, "TestInnerAbcReformSparseBlockColumn", 2, 0);
  ADDTEST(spdm3::TestInnerAbcReformSparseBlockColumn, "TestInnerAbcReformSparseBlockColumn", 4, 0);
  
  ADDTEST(spdm3::TestInnerAbcReformSparseBlockRow, "TestInnerAbcReformSparseBlockRow", 1, 0);
  ADDTEST(spdm3::TestInnerAbcReformSparseBlockRow, "TestInnerAbcReformSparseBlockRow", 2, 0);
  ADDTEST(spdm3::TestInnerAbcReformSparseBlockRow, "TestInnerAbcReformSparseBlockRow", 4, 0);
  
  ADDTEST(spdm3::TestInnerAbcShift1DDM3, "TestInnerAbcShift1DDM3", 1, 0);
  ADDTEST(spdm3::TestInnerAbcBcastDDM3, "TestInnerAbcBcastDDM3", 1, 0);
  ADDTEST(spdm3::TestInnerAbcBcastDDM3, "TestInnerAbcBcastDDM3", 2, 0);
  ADDTEST(spdm3::TestInnerAbcBcastDDM3, "TestInnerAbcBcastDDM3", 4, 0);

  ADDTEST(spdm3::TestAllBlkRowShift1, "TestAllBlkRowShift1", 1, 1);
  ADDTEST(spdm3::TestAllBlkRowShift1, "TestAllBlkRowShift1", 1, 2);
  ADDTEST(spdm3::TestAllBlkRowShift1, "TestAllBlkRowShift1", 2, 1);
  ADDTEST(spdm3::TestAllBlkRowShift1, "TestAllBlkRowShift1", 2, 2);
  ADDTEST(spdm3::TestAllBlkRowShift1, "TestAllBlkRowShift1", 1, 4);
  ADDTEST(spdm3::TestAllBlkRowShift1, "TestAllBlkRowShift1", 4, 1);
  ADDTEST(spdm3::TestAllBlkRowShift1, "TestAllBlkRowShift1", 2, 4);
  ADDTEST(spdm3::TestAllBlkRowShift1, "TestAllBlkRowShift1", 4, 2);
  ADDTEST(spdm3::TestAllBlkRowShift1, "TestAllBlkRowShift1", 4, 4);
  
  ADDTEST(spdm3::TestAllBlkRowShiftc, "TestAllBlkRowShiftc", 1, 1);
  ADDTEST(spdm3::TestAllBlkRowShiftc, "TestAllBlkRowShiftc", 1, 2);
  ADDTEST(spdm3::TestAllBlkRowShiftc, "TestAllBlkRowShiftc", 2, 1);
  ADDTEST(spdm3::TestAllBlkRowShiftc, "TestAllBlkRowShiftc", 2, 2);
  ADDTEST(spdm3::TestAllBlkRowShiftc, "TestAllBlkRowShiftc", 1, 4);
  ADDTEST(spdm3::TestAllBlkRowShiftc, "TestAllBlkRowShiftc", 4, 1);
  ADDTEST(spdm3::TestAllBlkRowShiftc, "TestAllBlkRowShiftc", 2, 4);
  ADDTEST(spdm3::TestAllBlkRowShiftc, "TestAllBlkRowShiftc", 4, 2);
  ADDTEST(spdm3::TestAllBlkRowShiftc, "TestAllBlkRowShiftc", 4, 4);
  
  ADDTEST(spdm3::TestInnerShiftcRotatingB, "TestInnerShiftcRotatingB", 1, 1);
  ADDTEST(spdm3::TestInnerShiftcRotatingB, "TestInnerShiftcRotatingB", 1, 2);
  ADDTEST(spdm3::TestInnerShiftcRotatingB, "TestInnerShiftcRotatingB", 2, 1);
  ADDTEST(spdm3::TestInnerShiftcRotatingB, "TestInnerShiftcRotatingB", 2, 2);
  ADDTEST(spdm3::TestInnerShiftcRotatingB, "TestInnerShiftcRotatingB", 1, 4);
  ADDTEST(spdm3::TestInnerShiftcRotatingB, "TestInnerShiftcRotatingB", 4, 1);
  ADDTEST(spdm3::TestInnerShiftcRotatingB, "TestInnerShiftcRotatingB", 2, 4);
  ADDTEST(spdm3::TestInnerShiftcRotatingB, "TestInnerShiftcRotatingB", 4, 2);
  ADDTEST(spdm3::TestInnerShiftcRotatingB, "TestInnerShiftcRotatingB", 4, 4);
  ADDTEST(spdm3::TestInnerShiftcRotatingB, "TestInnerShiftcRotatingB", 1, 8);
  ADDTEST(spdm3::TestInnerShiftcRotatingB, "TestInnerShiftcRotatingB", 8, 1);
  ADDTEST(spdm3::TestInnerShiftcRotatingB, "TestInnerShiftcRotatingB", 2, 8);
  ADDTEST(spdm3::TestInnerShiftcRotatingB, "TestInnerShiftcRotatingB", 8, 2);
  ADDTEST(spdm3::TestInnerShiftcRotatingB, "TestInnerShiftcRotatingB", 1, 16);
  ADDTEST(spdm3::TestInnerShiftcRotatingB, "TestInnerShiftcRotatingB", 16, 1);

  ADDTEST(spdm3::TestInnerShitfcRotatingBTranspose, "TestInnerShitfcRotatingBTranspose", 1, 1);
  ADDTEST(spdm3::TestInnerShitfcRotatingBTranspose, "TestInnerShitfcRotatingBTranspose", 1, 2);
  ADDTEST(spdm3::TestInnerShitfcRotatingBTranspose, "TestInnerShitfcRotatingBTranspose", 2, 1);
  ADDTEST(spdm3::TestInnerShitfcRotatingBTranspose, "TestInnerShitfcRotatingBTranspose", 2, 2);
  ADDTEST(spdm3::TestInnerShitfcRotatingBTranspose, "TestInnerShitfcRotatingBTranspose", 1, 4);
  ADDTEST(spdm3::TestInnerShitfcRotatingBTranspose, "TestInnerShitfcRotatingBTranspose", 4, 1);
  ADDTEST(spdm3::TestInnerShitfcRotatingBTranspose, "TestInnerShitfcRotatingBTranspose", 1, 8);
  ADDTEST(spdm3::TestInnerShitfcRotatingBTranspose, "TestInnerShitfcRotatingBTranspose", 8, 1);
  ADDTEST(spdm3::TestInnerShitfcRotatingBTranspose, "TestInnerShitfcRotatingBTranspose", 2, 4);
  ADDTEST(spdm3::TestInnerShitfcRotatingBTranspose, "TestInnerShitfcRotatingBTranspose", 4, 2);
  ADDTEST(spdm3::TestInnerShitfcRotatingBTranspose, "TestInnerShitfcRotatingBTranspose", 1, 16);
  ADDTEST(spdm3::TestInnerShitfcRotatingBTranspose, "TestInnerShitfcRotatingBTranspose", 16, 1);
  ADDTEST(spdm3::TestInnerShitfcRotatingBTranspose, "TestInnerShitfcRotatingBTranspose", 2, 8);
  ADDTEST(spdm3::TestInnerShitfcRotatingBTranspose, "TestInnerShitfcRotatingBTranspose", 8, 2);
  ADDTEST(spdm3::TestInnerShitfcRotatingBTranspose, "TestInnerShitfcRotatingBTranspose", 4, 4);

  ADDTEST(spdm3::TestReduceDiagonalWeirdLayoutRotatingB, "TestReduceDiagonalWeirdLayoutRotatingB", 1, 1);
  ADDTEST(spdm3::TestReduceDiagonalWeirdLayoutRotatingB, "TestReduceDiagonalWeirdLayoutRotatingB", 1, 2);
  ADDTEST(spdm3::TestReduceDiagonalWeirdLayoutRotatingB, "TestReduceDiagonalWeirdLayoutRotatingB", 2, 1);
  ADDTEST(spdm3::TestReduceDiagonalWeirdLayoutRotatingB, "TestReduceDiagonalWeirdLayoutRotatingB", 2, 2);
  ADDTEST(spdm3::TestReduceDiagonalWeirdLayoutRotatingB, "TestReduceDiagonalWeirdLayoutRotatingB", 2, 2);
  ADDTEST(spdm3::TestReduceDiagonalWeirdLayoutRotatingB, "TestReduceDiagonalWeirdLayoutRotatingB", 1, 4);
  ADDTEST(spdm3::TestReduceDiagonalWeirdLayoutRotatingB, "TestReduceDiagonalWeirdLayoutRotatingB", 4, 1);
  ADDTEST(spdm3::TestReduceDiagonalWeirdLayoutRotatingB, "TestReduceDiagonalWeirdLayoutRotatingB", 1, 8);
  ADDTEST(spdm3::TestReduceDiagonalWeirdLayoutRotatingB, "TestReduceDiagonalWeirdLayoutRotatingB", 8, 1);
  ADDTEST(spdm3::TestReduceDiagonalWeirdLayoutRotatingB, "TestReduceDiagonalWeirdLayoutRotatingB", 2, 4);
  ADDTEST(spdm3::TestReduceDiagonalWeirdLayoutRotatingB, "TestReduceDiagonalWeirdLayoutRotatingB", 4, 2);
  ADDTEST(spdm3::TestReduceDiagonalWeirdLayoutRotatingB, "TestReduceDiagonalWeirdLayoutRotatingB", 1, 16);
  ADDTEST(spdm3::TestReduceDiagonalWeirdLayoutRotatingB, "TestReduceDiagonalWeirdLayoutRotatingB", 16, 1);
  ADDTEST(spdm3::TestReduceDiagonalWeirdLayoutRotatingB, "TestReduceDiagonalWeirdLayoutRotatingB", 2, 8);
  ADDTEST(spdm3::TestReduceDiagonalWeirdLayoutRotatingB, "TestReduceDiagonalWeirdLayoutRotatingB", 8, 2);
  ADDTEST(spdm3::TestReduceDiagonalWeirdLayoutRotatingB, "TestReduceDiagonalWeirdLayoutRotatingB", 4, 4);
  
  ADDTEST(spdm3::TestOpDiagonalWeirdLayoutRotatingB, "TestOpDiagonalWeirdLayoutRotatingB", 1, 1);
  ADDTEST(spdm3::TestOpDiagonalWeirdLayoutRotatingB, "TestOpDiagonalWeirdLayoutRotatingB", 1, 2);
  ADDTEST(spdm3::TestOpDiagonalWeirdLayoutRotatingB, "TestOpDiagonalWeirdLayoutRotatingB", 2, 1);
  ADDTEST(spdm3::TestOpDiagonalWeirdLayoutRotatingB, "TestOpDiagonalWeirdLayoutRotatingB", 2, 2);
  ADDTEST(spdm3::TestOpDiagonalWeirdLayoutRotatingB, "TestOpDiagonalWeirdLayoutRotatingB", 1, 4);
  ADDTEST(spdm3::TestOpDiagonalWeirdLayoutRotatingB, "TestOpDiagonalWeirdLayoutRotatingB", 4, 1);
  ADDTEST(spdm3::TestOpDiagonalWeirdLayoutRotatingB, "TestOpDiagonalWeirdLayoutRotatingB", 1, 8);
  ADDTEST(spdm3::TestOpDiagonalWeirdLayoutRotatingB, "TestOpDiagonalWeirdLayoutRotatingB", 8, 1);
  ADDTEST(spdm3::TestOpDiagonalWeirdLayoutRotatingB, "TestOpDiagonalWeirdLayoutRotatingB", 2, 4);
  ADDTEST(spdm3::TestOpDiagonalWeirdLayoutRotatingB, "TestOpDiagonalWeirdLayoutRotatingB", 4, 2);
  ADDTEST(spdm3::TestOpDiagonalWeirdLayoutRotatingB, "TestOpDiagonalWeirdLayoutRotatingB", 1, 16);
  ADDTEST(spdm3::TestOpDiagonalWeirdLayoutRotatingB, "TestOpDiagonalWeirdLayoutRotatingB", 16, 1);
  ADDTEST(spdm3::TestOpDiagonalWeirdLayoutRotatingB, "TestOpDiagonalWeirdLayoutRotatingB", 2, 8);
  ADDTEST(spdm3::TestOpDiagonalWeirdLayoutRotatingB, "TestOpDiagonalWeirdLayoutRotatingB", 8, 2);
  ADDTEST(spdm3::TestOpDiagonalWeirdLayoutRotatingB, "TestOpDiagonalWeirdLayoutRotatingB", 4, 4);
  
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMRowDenseDense", 1, 1);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMRowDenseDense", 1, 2);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMRowDenseDense", 1, 4);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMRowDenseDense", 1, 8);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMRowDenseDense", 1, 16);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMRowDenseDense", 2, 1);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMRowDenseDense", 2, 2);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMRowDenseDense", 2, 4);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMRowDenseDense", 2, 8);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMRowDenseDense", 4, 1);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMRowDenseDense", 4, 2);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMRowDenseDense", 4, 4);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMRowDenseDense", 8, 1);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMRowDenseDense", 8, 2);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMRowDenseDense", 16, 1);

  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMRowSparseDense", 1, 1);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMRowSparseDense", 1, 2);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMRowSparseDense", 1, 4);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMRowSparseDense", 1, 8);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMRowSparseDense", 1, 16);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMRowSparseDense", 2, 1);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMRowSparseDense", 2, 2);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMRowSparseDense", 2, 4);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMRowSparseDense", 2, 8);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMRowSparseDense", 4, 1);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMRowSparseDense", 4, 2);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMRowSparseDense", 4, 4);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMRowSparseDense", 8, 1);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMRowSparseDense", 8, 2);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMRowSparseDense", 16, 1);
  
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMColDenseDense", 1, 1);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMColDenseDense", 1, 2);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMColDenseDense", 1, 4);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMColDenseDense", 1, 8);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMColDenseDense", 1, 16);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMColDenseDense", 2, 1);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMColDenseDense", 2, 2);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMColDenseDense", 2, 4);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMColDenseDense", 2, 8);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMColDenseDense", 4, 1);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMColDenseDense", 4, 2);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMColDenseDense", 4, 4);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMColDenseDense", 8, 1);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMColDenseDense", 8, 2);
  ADDTEST(spdm3::TestMMRowDenseDense, "TestMMColDenseDense", 16, 1);
  
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMColSparseDense", 1, 1);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMColSparseDense", 1, 2);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMColSparseDense", 1, 4);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMColSparseDense", 1, 8);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMColSparseDense", 1, 16);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMColSparseDense", 2, 1);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMColSparseDense", 2, 2);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMColSparseDense", 2, 4);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMColSparseDense", 2, 8);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMColSparseDense", 4, 1);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMColSparseDense", 4, 2);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMColSparseDense", 4, 4);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMColSparseDense", 8, 1);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMColSparseDense", 8, 2);
  ADDTEST(spdm3::TestMMRowSparseDense, "TestMMColSparseDense", 16, 1);

  ADDTEST(spdm3::TestMMInnerASumReduceDenseDense, "TestMMInnerASumReduceDenseDense", 1, 1);
  ADDTEST(spdm3::TestMMInnerASumReduceDenseDense, "TestMMInnerASumReduceDenseDense", 1, 2);
  ADDTEST(spdm3::TestMMInnerASumReduceDenseDense, "TestMMInnerASumReduceDenseDense", 1, 4);
  ADDTEST(spdm3::TestMMInnerASumReduceDenseDense, "TestMMInnerASumReduceDenseDense", 1, 8);
  ADDTEST(spdm3::TestMMInnerASumReduceDenseDense, "TestMMInnerASumReduceDenseDense", 1, 16);
  ADDTEST(spdm3::TestMMInnerASumReduceDenseDense, "TestMMInnerASumReduceDenseDense", 2, 1);
  ADDTEST(spdm3::TestMMInnerASumReduceDenseDense, "TestMMInnerASumReduceDenseDense", 2, 2);
  ADDTEST(spdm3::TestMMInnerASumReduceDenseDense, "TestMMInnerASumReduceDenseDense", 2, 4);
  ADDTEST(spdm3::TestMMInnerASumReduceDenseDense, "TestMMInnerASumReduceDenseDense", 2, 8);
  ADDTEST(spdm3::TestMMInnerASumReduceDenseDense, "TestMMInnerASumReduceDenseDense", 4, 1);
  ADDTEST(spdm3::TestMMInnerASumReduceDenseDense, "TestMMInnerASumReduceDenseDense", 4, 2);
  ADDTEST(spdm3::TestMMInnerASumReduceDenseDense, "TestMMInnerASumReduceDenseDense", 4, 4);
  ADDTEST(spdm3::TestMMInnerASumReduceDenseDense, "TestMMInnerASumReduceDenseDense", 8, 1);
  ADDTEST(spdm3::TestMMInnerASumReduceDenseDense, "TestMMInnerASumReduceDenseDense", 8, 2);
  ADDTEST(spdm3::TestMMInnerASumReduceDenseDense, "TestMMInnerASumReduceDenseDense", 16, 1);
  
  ADDTEST(spdm3::TestMMInnerADenseDense, "TestMMInnerADenseDense", 1, 1);
  ADDTEST(spdm3::TestMMInnerADenseDense, "TestMMInnerADenseDense", 1, 2);
  ADDTEST(spdm3::TestMMInnerADenseDense, "TestMMInnerADenseDense", 1, 4);
  ADDTEST(spdm3::TestMMInnerADenseDense, "TestMMInnerADenseDense", 1, 8);
  ADDTEST(spdm3::TestMMInnerADenseDense, "TestMMInnerADenseDense", 1, 16);
  ADDTEST(spdm3::TestMMInnerADenseDense, "TestMMInnerADenseDense", 2, 1);
  ADDTEST(spdm3::TestMMInnerADenseDense, "TestMMInnerADenseDense", 2, 2);
  ADDTEST(spdm3::TestMMInnerADenseDense, "TestMMInnerADenseDense", 2, 4);
  ADDTEST(spdm3::TestMMInnerADenseDense, "TestMMInnerADenseDense", 2, 8);
  ADDTEST(spdm3::TestMMInnerADenseDense, "TestMMInnerADenseDense", 4, 1);
  ADDTEST(spdm3::TestMMInnerADenseDense, "TestMMInnerADenseDense", 4, 2);
  ADDTEST(spdm3::TestMMInnerADenseDense, "TestMMInnerADenseDense", 4, 4);
  ADDTEST(spdm3::TestMMInnerADenseDense, "TestMMInnerADenseDense", 8, 1);
  ADDTEST(spdm3::TestMMInnerADenseDense, "TestMMInnerADenseDense", 8, 2);
  ADDTEST(spdm3::TestMMInnerADenseDense, "TestMMInnerADenseDense", 16, 1);

  ADDTEST(spdm3::TestMMInnerASparseDense, "TestMMInnerASparseDense", 1, 1);
  ADDTEST(spdm3::TestMMInnerASparseDense, "TestMMInnerASparseDense", 1, 2);
  ADDTEST(spdm3::TestMMInnerASparseDense, "TestMMInnerASparseDense", 1, 4);
  ADDTEST(spdm3::TestMMInnerASparseDense, "TestMMInnerASparseDense", 1, 8);
  ADDTEST(spdm3::TestMMInnerASparseDense, "TestMMInnerASparseDense", 1, 16);
  ADDTEST(spdm3::TestMMInnerASparseDense, "TestMMInnerASparseDense", 2, 1);
  ADDTEST(spdm3::TestMMInnerASparseDense, "TestMMInnerASparseDense", 2, 2);
  ADDTEST(spdm3::TestMMInnerASparseDense, "TestMMInnerASparseDense", 2, 4);
  ADDTEST(spdm3::TestMMInnerASparseDense, "TestMMInnerASparseDense", 2, 8);
  ADDTEST(spdm3::TestMMInnerASparseDense, "TestMMInnerASparseDense", 4, 1);
  ADDTEST(spdm3::TestMMInnerASparseDense, "TestMMInnerASparseDense", 4, 2);
  ADDTEST(spdm3::TestMMInnerASparseDense, "TestMMInnerASparseDense", 4, 4);
  ADDTEST(spdm3::TestMMInnerASparseDense, "TestMMInnerASparseDense", 8, 1);
  ADDTEST(spdm3::TestMMInnerASparseDense, "TestMMInnerASparseDense", 8, 2);
  ADDTEST(spdm3::TestMMInnerASparseDense, "TestMMInnerASparseDense", 16, 1);
  
//  ADDTEST(spdm3::TestInnerShiftcRotatingBAsync, "TestInnerShiftcRotatingBAsync", 1, 1);
//  ADDTEST(spdm3::TestDummy, "TestDummy", 1, 1);
  
  MPI_Init(&argc, &argv);
  spdm3::T.Init(MPI_COMM_WORLD);
  
  int p = 1, rank = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (p != 16) {
    if (rank == 0) {
      std::cout << "This test must be run with 16 MPI processes. "
                << "Terminating..." << std::endl;
    }
    exit(1);
  }
  
  // Runs tests.
  int failed = 0;
  for (auto test : tests) {
    int layers_1 = std::get<2>(test);
    int layers_2 = std::get<3>(test);
    bool res = std::get<0>(test)(layers_1, layers_2);
    if (rank == 0) {
      MPI_Reduce(MPI_IN_PLACE, &res, 1, MPI_BYTE, MPI_BAND, 0, MPI_COMM_WORLD);
      std::stringstream ss;
      ss << std::get<1>(test) << "C" << layers_1 << "-" << layers_2;
      std::cout << std::setw(50) << ss.str() << ": ";
      if (res) {
        std::cout << "PASSED." << std::endl;
      } else {
        ++failed;
        std::cout << "FAILED." << std::endl;
      }
    } else {
      MPI_Reduce(&res, NULL, 1, MPI_BYTE, MPI_BAND, 0, MPI_COMM_WORLD);
    }
  }
  
  // Summarizes.
  if (rank == 0) {
    if (!failed)
      std::cout << "All " << tests.size() << " tests passed." << std::endl;
    else
      std::cout << failed << " out of " << tests.size()
                << " tests failed." << std::endl;
  }

  MPI_Finalize();
  return 0;
}
