#include "comm.h"

#include <cstdio>
#include <cstdlib>
#include <functional>
#include <iomanip>
#include <iostream>
#include <list>
#include <utility>

#include "utility.h"

namespace spdm3 {

bool Test1DBlockRow1Layer() {
  bool res = true;
  int allzeroes[32], series[32];
  fill_vec(allzeroes, 32, 0);
  fill_series(series, 32, 0);
  
  Comm *comm = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 1);
  int x, xs[32];
  x = comm[LAYER_ROW].rank;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[LAYER_ROW].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, allzeroes, 32);
  
  x = comm[LAYER_ROW].id;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[LAYER_ROW].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, series, 32);
  
  x = comm[LAYER_COL].id;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[LAYER_COL].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, allzeroes, 32);
  
  x = comm[LAYER_COL].rank;
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, series, 32);

  delete [] comm;
  return res;
}

bool Test1DBlockRow4Layers() {
  bool res = true;
  int layers[32], teams[32];
  for (int  i = 0; i < 32; i+= 4) {
    fill_series(layers + i, 4, 0);
    fill_vec(teams + i, 4, i);
  }
  
  Comm *comm = GetComms(MPI_COMM_WORLD, ONED_BLOCK_ROW, 4);
  int x, xs[32];
  x = comm[WORLD].rank;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[LAYER_COL].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, layers, 32);
  
  x = comm[WORLD].rank;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[TEAM].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, teams, 32);
  
  delete [] comm;
  return res;
}

bool Test1DBlockCol1Layer() {
  bool res = true;
  int allzeroes[32], series[32];
  fill_vec(allzeroes, 32, 0);
  fill_series(series, 32, 0);
  
  Comm *comm = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, 1);
  int x, xs[32];
  x = comm[LAYER_COL].rank;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[LAYER_COL].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, allzeroes, 32);
  
  x = comm[LAYER_COL].id;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[LAYER_COL].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, series, 32);
  
  x = comm[LAYER_ROW].id;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[LAYER_ROW].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, allzeroes, 32);
  
  x = comm[LAYER_ROW].rank;
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, series, 32);

  delete [] comm;
  return res;
}

bool Test1DBlockCol4Layers() {
  bool res = true;
  int layers[32], teams[32];
  for (int  i = 0; i < 32; i+= 4) {
    fill_series(layers + i, 4, 0);
    fill_vec(teams + i, 4, i);
  }
  
  Comm *comm = GetComms(MPI_COMM_WORLD, ONED_BLOCK_COLUMN, 4);
  int x, xs[32];
  x = comm[WORLD].rank;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[LAYER_ROW].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, layers, 32);
  
  x = comm[WORLD].rank;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[TEAM].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, teams, 32);
  
  delete [] comm;
  return res;
}

bool Test2DRowMajor1Layer() {
  bool res = true;
  int rows[32], cols[32];
  for (int i = 0; i < 32; i += 8) {
    fill_vec(rows + i, 8, i);
    fill_series(cols + i, 8, 0);
  }
  Comm *comm = GetComms(MPI_COMM_WORLD, TWOD_ROWMAJOR, 1);
  int x, xs[32];
  x = comm[WORLD].rank;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[LAYER_ROW].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, rows, 32);
  
  x = comm[WORLD].rank;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[LAYER_COL].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, cols, 32);
  
  delete [] comm;
  return res;
}

bool Test2DRowMajor2Layers() {
  bool res = true;
  int rows[32], cols[32], teams[32];
  for (int i = 0; i < 32; i += 8) {
    for (int j = 0; j < 8; j += 2) {
      rows[i+j]   = i;
      rows[i+j+1] = i+1;
      teams[i+j] = teams[i+j+1] = i+j;
    }
    fill_series(cols + i, 8, 0);
  }
  Comm *comm = GetComms(MPI_COMM_WORLD, TWOD_ROWMAJOR, 2);
  int x, xs[32];
  x = comm[WORLD].rank;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[LAYER_ROW].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, rows, 32);
  
  x = comm[WORLD].rank;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[LAYER_COL].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, cols, 32);
  
  x = comm[WORLD].rank;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[TEAM].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, teams, 32);
  
  delete [] comm;
  return res;
}

bool Test2DColMajor1Layer() {
  bool res = true;
  int rows[32], cols[32];
  for (int i = 0; i < 32; i += 4) {
    fill_series(rows + i, 4, 0);
    fill_vec(cols + i, 4, i);
  }
  Comm *comm = GetComms(MPI_COMM_WORLD, TWOD_COLMAJOR, 1);
  int x, xs[32];
  x = comm[WORLD].rank;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[LAYER_ROW].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, rows, 32);
  
  x = comm[WORLD].rank;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[LAYER_COL].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, cols, 32);
  
  delete [] comm;
  return res;
}

bool Test2DColMajor2Layers() {
  bool res = true;
  int rows[32], cols[32], teams[32];
  for (int i = 0; i < 32; i += 8) {
    fill_series(rows + i, 8, 0);
    fill_vec(cols + i, 4, i);
    fill_vec(cols + i + 4, 4, i + 4);
    fill_series(teams + i, 4, i);
    fill_series(teams + i + 4, 4, i);
  }
  Comm *comm = GetComms(MPI_COMM_WORLD, TWOD_COLMAJOR, 2);
  int x, xs[32];
  x = comm[WORLD].rank;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[LAYER_ROW].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, rows, 32);
  
  x = comm[WORLD].rank;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[LAYER_COL].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, cols, 32);
  
  x = comm[WORLD].rank;
  MPI_Bcast(&x, 1, MPI_INT, 0, comm[TEAM].comm);
  MPI_Allgather(&x, 1, MPI_INT, xs, 1, MPI_INT, comm[WORLD].comm);
  res &= vec_equal(xs, teams, 32);
  
  delete [] comm;
  return res;
}

bool TestMerging() {
  Comm *c1 = GetComms(MPI_COMM_WORLD, TWOD_ROWMAJOR, 1);
  Comm *c4 = GetComms(MPI_COMM_WORLD, TWOD_ROWMAJOR, 4);
//  printf("%d: %d %d; %d %d %d; %d %d %d; %d %d\n", c1[WORLD].rank,
//      c1[LAYER].rank, c4[LAYER].rank,
//      c1[LAYER_COL].rank, c4[LAYER_COL].rank, c4[LAYER_ROW].id,
//      c1[LAYER_ROW].rank, c4[LAYER_ROW].rank, c4[LAYER_COL].id,
//      c1[TEAM].rank, c4[TEAM].rank);
  bool res = true;
  int rank = c1[LAYER].rank;
  if (rank == 0 || rank == 1 || rank == 8 || rank == 9) {
    res &= (c4[LAYER].rank == 0);
    res &= (c4[LAYER_ROW].id == 0);
    res &= (c4[LAYER_COL].id == 0);
  } else if (rank == 2 || rank == 3 || rank == 10 || rank == 11) {
    res &= (c4[LAYER].rank == 1);
    res &= (c4[LAYER_ROW].id == 0);
    res &= (c4[LAYER_COL].id == 1);
  } else if (rank == 4 || rank == 5 || rank == 12 || rank == 13) {
    res &= (c4[LAYER].rank == 2);
    res &= (c4[LAYER_ROW].id == 0);
    res &= (c4[LAYER_COL].id == 2);
  } else if (rank == 6 || rank == 7 || rank == 14 || rank == 15) {
    res &= (c4[LAYER].rank == 3);
    res &= (c4[LAYER_ROW].id == 0);
    res &= (c4[LAYER_COL].id == 3);
  } else if (rank == 16 || rank == 17 || rank == 24 || rank == 25) {
    res &= (c4[LAYER].rank == 4);
    res &= (c4[LAYER_ROW].id == 1);
    res &= (c4[LAYER_COL].id == 0);
  } else if (rank == 18 || rank == 19 || rank == 26 || rank == 27) {
    res &= (c4[LAYER].rank == 5);
    res &= (c4[LAYER_ROW].id == 1);
    res &= (c4[LAYER_COL].id == 1);
  } else if (rank == 20 || rank == 21 || rank == 28 || rank == 29) {
    res &= (c4[LAYER].rank == 6);
    res &= (c4[LAYER_ROW].id == 1);
    res &= (c4[LAYER_COL].id == 2);
  } else if (rank == 22 || rank == 23 || rank == 30 || rank == 31) {
    res &= (c4[LAYER].rank == 7);
    res &= (c4[LAYER_ROW].id == 1);
    res &= (c4[LAYER_COL].id == 3);
  }
  return res;
}

}  // namespace spdm3

#define ADDTEST(fn, name) tests.push_back(std::make_pair(fn, name));

int main(int argc, char* argv[]) {
  // Registers test cases.
  std::list< std::pair<std::function<bool()>, std::string> > tests;
  
  ADDTEST(spdm3::Test1DBlockRow1Layer, "Test1DBlockRow1Layer");
  ADDTEST(spdm3::Test1DBlockRow4Layers, "Test1DBlockRow4Layers");
  ADDTEST(spdm3::Test1DBlockCol1Layer, "Test1DBlockCol1Layer");
  ADDTEST(spdm3::Test1DBlockCol4Layers, "Test1DBlockCol4Layers");
  ADDTEST(spdm3::Test2DRowMajor1Layer, "Test2DRowMajor1Layer");
  ADDTEST(spdm3::Test2DRowMajor2Layers, "Test2DRowMajor2Layers");
  ADDTEST(spdm3::Test2DColMajor1Layer, "Test2DColMajor1Layer");
  ADDTEST(spdm3::Test2DColMajor2Layers, "Test2DColMajor2Layers");
  ADDTEST(spdm3::TestMerging, "TestMerging");
  
  MPI_Init(&argc, &argv);
  int p = 1, rank = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &p);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  if (p != 32) {
    std::cout << "This test must be run with 32 MPI processes. "
              << "Terminating..." << std::endl;
    exit(1);
  }
  
  // Runs tests.
  int failed = 0;
  for (auto test : tests) {
    bool res = test.first();
    if (rank == 0) {
      MPI_Reduce(MPI_IN_PLACE, &res, 1, MPI_BYTE, MPI_BAND, 0, MPI_COMM_WORLD);
      std::cout << std::setw(50) << test.second << ": ";
      if (res) {
        std::cout << "PASSED." << std::endl;
      } else {
        ++failed;
        std::cout << "FAILED." << std::endl;
      }
    } else {
      MPI_Reduce(&res, NULL, 1, MPI_BYTE, MPI_BAND, 0, MPI_COMM_WORLD);
    }
  }
  
  // Summarizes.
  if (rank == 0) {
    if (!failed)
      std::cout << "All " << tests.size() << " tests passed." << std::endl;
    else
      std::cout << failed << " out of " << tests.size()
                << " tests failed." << std::endl;
  }

  MPI_Finalize();
  return 0;
}